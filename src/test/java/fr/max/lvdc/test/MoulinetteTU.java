
package fr.max.lvdc.test;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.Test;


public class MoulinetteTU {

    private final String packageUrlTest = "fr.max.lvdc.test";
    private int i = 0;

    @Test
    public void testMoulinette() throws ClassNotFoundException {

	// on donne le chemin de l'artifact.
	final File src = new File("./src/main/java");

	for (final File currentFile : recurisf(src)) {

	    if (currentFile.getParentFile().getName().equals("impl")) {

		// System.out.println(" File :" + currentFile.getName());
		analyseMethodeFile(currentFile);
	    }
	}

	System.out.println("nombre de methode manquante :" + i);

    }

    /**
     * @param currentFile
     *            File
     * @throws ClassNotFoundException
     */
    private void analyseMethodeFile(final File currentFile)
	    throws ClassNotFoundException {

	final String path =
		currentFile.getPath().replace(".\\src\\main\\java", "")
			.replace("\\", ".").replaceAll(".java", "")
			.substring(1);

	final Class<?> result =
		ClassLoader.getSystemClassLoader().loadClass(path);

	final Method[] m = Object.class.getDeclaredMethods();
	final Class<?> m2 = result.getSuperclass();

	for (final Method currentMethod : result.getMethods()) {

	    boolean test = true;

	    for (int i = 0; i < m.length; i++) {

		if (m[i].equals(currentMethod)) {
		    test = false;
		}
	    }

	    for (int i = 0; i < m2.getMethods().length; i++) {

		if (m2.getMethods()[i].equals(currentMethod)) {
		    test = false;
		}
	    }

	    if (test) {
		final StringBuffer methode = new StringBuffer();

		methode.append(formatParametre(currentMethod.getReturnType()
			.getName()));
		methode.append(" ");
		methode.append(currentMethod.getName());
		methode.append(rechercheParametres(currentMethod
			.getParameterTypes()));
		// System.out.println(" Method :" + methode);
		// System.out.println(" name :" + currentMethod.getName());

		existeTUMethode(currentMethod.getName(), currentFile);
	    }
	}

    }

    /**
     * @param currentFile
     *            File
     * @param methodName
     *            String
     * @throws ClassNotFoundException
     *             ClassNotFoundException
     */
    private void analyseMethodeFileExist(final File currentFile,
	    final String methodName) throws ClassNotFoundException {

	final String pathTest =
		currentFile.getPath().replace(".\\src\\test\\java", "")
			.replace("\\", ".").replaceAll(".java", "")
			.substring(1);

	final Class<?> resultTest =
		ClassLoader.getSystemClassLoader().loadClass(pathTest);

	boolean test = false;

	for (final Method currentMethod : resultTest.getMethods()) {

	    if (currentMethod.getName().toLowerCase()
		    .equals(("test" + methodName).toLowerCase())
		    || currentMethod.getName().toLowerCase()
			    .equals((methodName + "test").toLowerCase())
		    || currentMethod.getName().toLowerCase()
			    .equals(("set" + methodName).toLowerCase())) {

		test = true;
		break;

	    }
	}

	if (!test) {

	    System.out.println(currentFile.getName()
		    + " -> ajouter methode : @Test public void test"
		    + methodName + "() {}");

	    i++;

	}
    }

    /**
     * Verifie si methode existe dans TU.
     * 
     * @param name
     *            String
     * @param currentFile
     *            File
     * @param String
     *            retourne le nom de la methode manquante
     * @throws ClassNotFoundException
     */
    private String existeTUMethode(final String name, final File currentFile)
	    throws ClassNotFoundException {

	final StringBuilder result = new StringBuilder();

	File file =
		new File("./src/test/java"
			+ "/"
			+ packageUrlTest.replace(".", "/")
			+ "/"
			+ currentFile.getParentFile().getParentFile()
				.getName() + "/Test" + currentFile.getName());

	if (!file.exists()) {

	    file =
		    new File("./src/test/java"
			    + "/"
			    + packageUrlTest.replace(".", "/")
			    + "/"
			    + currentFile.getParentFile().getParentFile()
				    .getName() + "/" + currentFile.getName()
			    + "Test");
	}

	if (file.isFile()) {

	    analyseMethodeFileExist(file, name);

	}

	return result.toString();
    }

    /**
     * @param classes
     *            Class[]
     * @return String rechercheParametres
     */
    @SuppressWarnings("rawtypes")
    private String rechercheParametres(final Class[] classes) {

	final StringBuffer param = new StringBuffer("(");

	for (int i = 0; i < classes.length; i++) {

	    param.append(formatParametre(classes[i].getName()));
	    if (i < classes.length - 1)
		param.append(", ");

	}
	param.append(")");

	return param.toString();

    }

    private String formatParametre(final String s) {

	if (s.charAt(0) == '[') {

	    final StringBuffer param = new StringBuffer("");

	    int dimension = 0;
	    while (s.charAt(dimension) == '[')
		dimension++;

	    switch (s.charAt(dimension)) {
		case 'B':
		    param.append("byte");
		    break;
		case 'C':
		    param.append("char");
		    break;
		case 'D':
		    param.append("double");
		    break;
		case 'F':
		    param.append("float");
		    break;
		case 'I':
		    param.append("int");
		    break;
		case 'J':
		    param.append("long");
		    break;
		case 'S':
		    param.append("short");
		    break;
		case 'Z':
		    param.append("boolean");
		    break;
		case 'L':
		    param.append(s.substring(dimension + 1, s.indexOf(";")));
	    }

	    for (int i = 0; i < dimension; i++)
		param.append("[]");

	    return param.toString();
	} else
	    return s;

    }

    /**
     * Recupere l'ensemble des fichier d'un repertoire.
     * 
     * @param file
     *            File
     * @return java.util.List<File> recurisf
     */
    private java.util.List<File> recurisf(final File file) {

	final java.util.List<File> result = new ArrayList<>();

	if (file.isFile()) {
	    result.add(file);
	    return result;
	} else {
	    for (final File currentFile : file.listFiles()) {
		result.addAll(recurisf(currentFile));
	    }
	}

	return result;

    }

}

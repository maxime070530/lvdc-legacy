
package fr.max.lvdc.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class CreaUserTU {

    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userManagerFacade;

    @Test
    @Transactional(propagation = Propagation.REQUIRED)
    public void CreaUser() {

	List<JoueurPOJO> listeJoueurs = this.communFacade.findAllJoueur();
	UserPOJO user = new UserPOJO();

	RolePOJO role = new RolePOJO();
	role = this.userManagerFacade.getRole(2);

	for (JoueurPOJO joueur : listeJoueurs) {

	    user.setMdp(joueur.getNomJoueur());
	    user.setJoueur(joueur);

	    this.userManagerFacade.saveMonCompte(user);
	}
    }

}


package fr.max.lvdc.bean.admin;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.CloseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.core.GetParameterUrl;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@Component("beanUser")
@Scope("view")
@Lazy(true)
public class BeanUser extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userManagerFacade;

    @GetParameterUrl(paramName = "idUser")
    private Integer idUser;

    private UserPOJO user;

    private String newMdp;
    private String verifNewMdp;
    private String oldMdp;

    private List<RolePOJO> listeRoles;

    @PostConstruct
    public void init() {
	this.user = this.userManagerFacade.findUserById(idUser);
	this.listeRoles = this.userManagerFacade.findAllRole();
    }

    public UserPOJO getUser() {
	return user;
    }

    public void setUser(UserPOJO user) {
	this.user = user;
    }

    public String getNewMdp() {
	return newMdp;
    }

    public void setNewMdp(String newMdp) {
	this.newMdp = newMdp;
    }

    public String getVerifNewMdp() {
	return verifNewMdp;
    }

    public void setVerifNewMdp(String verifNewMdp) {
	this.verifNewMdp = verifNewMdp;
    }

    public String getOldMdp() {
	return oldMdp;
    }

    public void setOldMdp(String oldMdp) {
	this.oldMdp = oldMdp;
    }

    public List<RolePOJO> getListeRoles() {
	return listeRoles;
    }

    public void changeMdp() {

	this.user.setMdp(fr.max.lvdc.core.Utils.encodeBCrypt(newMdp));
	this.userManagerFacade.saveMonCompte(this.user);
	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage("Changement de mot de passe r�ussi", " "));

    }

    public void saveMonCompte() {

	this.userManagerFacade.saveMonCompte(this.user);
	setModificationGeneral(false);

	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage("Sauvegarde effectu�e de l'utilisateur : "
			+ this.user.getNom(), " "));

    }

    public void initDialogMdp(final CloseEvent event) {

	this.oldMdp = null;
	this.newMdp = null;
	this.verifNewMdp = null;

	resetInputValues(event.getComponent().getChildren());

    }

    /**
     * @param event
     *            AjaxBehaviorEvent
     */
    public void openDialogMdp(final AjaxBehaviorEvent event) {

	this.oldMdp = null;
	this.newMdp = null;
	this.verifNewMdp = null;

    }

    public String delete() {

	if (this.user.getJoueur().getListeInscriptions().isEmpty()) {
	    this.userManagerFacade.deleteUser(this.user);

	    return "USERS";
	} else {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Le joueur est inscrit � des tournois", " "));
	}

	return null;
    }

}

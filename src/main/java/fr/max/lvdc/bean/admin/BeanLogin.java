
package fr.max.lvdc.bean.admin;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@Component("beanLogin")
@Scope("view")
@Lazy(true)
public class BeanLogin extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 923822096441121452L;

    @SuppressWarnings("unused")
    /** The Constant LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(BeanLogin.class);

    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userFacade;

    private UserPOJO user = new UserPOJO();
    private JoueurPOJO joueur = null;
    private List<ClubPOJO> listeClubs = null;

    @PostConstruct
    public void init() {
	this.listeClubs = this.communFacade.findAllClub();

    }

    public UserPOJO getUser() {
	return user;
    }

    public void setUser(UserPOJO user) {
	this.user = user;
    }

    public JoueurPOJO getJoueur() {
	return joueur;
    }

    public void setJoueur(JoueurPOJO joueur) {
	this.joueur = joueur;
    }

    public List<ClubPOJO> getListeClubs() {
	return listeClubs;
    }

    public String save() {

	try {

	    // this.communFacade.saveJoueur(this.joueur);

	    this.userFacade.saveMonCompte(this.user);

	    // this.acces.getRole().setId(2);
	    // this.acces.setUser(this.user);
	    // this.userFacade.saveRoleUtil(acces);

	    return "LOGIN";

	} catch (final Exception e) {

	    // message d erreur
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    e.getMessage(), " "));
	}

	return null;

    }
}

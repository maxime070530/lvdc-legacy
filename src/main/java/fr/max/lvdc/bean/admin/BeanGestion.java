
package fr.max.lvdc.bean.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.dto.ws.JoueurWsDTO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@Component("beanGestion")
@Scope("view")
@Lazy(true)
public class BeanGestion extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userManagerFacade;

    private UserPOJO user;
    private List<JoueurPOJO> listeJoueurs = new ArrayList<>();
    private List<JoueurPOJO> listeJoueursSansCompte = new ArrayList<>();

    private List<JoueurPOJO> selectedJoueurs = new ArrayList<>();

    private List<UserPOJO> listeUsers = new ArrayList<UserPOJO>();

    private List<JoueurWsDTO> listeJoueursAutocomplete;

    private UserPOJO selectedUser;

    private JoueurWsDTO joueurWs;

    @PostConstruct
    public void init() {
	this.listeJoueursSansCompte =
		this.communFacade.findListeJoueursWhereNoCompte();
	this.listeUsers = this.userManagerFacade.findAllUsers();

    }

    public UserPOJO getUser() {
	return user;
    }

    public void setUser(UserPOJO user) {
	this.user = user;
    }

    public List<JoueurPOJO> getListeJoueurs() {
	return listeJoueurs;
    }

    public List<JoueurPOJO> getListeJoueursSansCompte() {
	return listeJoueursSansCompte;
    }

    public List<JoueurPOJO> getSelectedJoueurs() {
	return selectedJoueurs;
    }

    public void setSelectedJoueurs(List<JoueurPOJO> selectedJoueurs) {
	this.selectedJoueurs = selectedJoueurs;
    }

    public UserPOJO getSelectedUser() {
	return selectedUser;
    }

    public void setSelectedUser(UserPOJO selectedUser) {
	this.selectedUser = selectedUser;
    }

    public List<UserPOJO> getListeUsers() {
	return listeUsers;
    }

    public JoueurWsDTO getJoueurWs() {
	return joueurWs;
    }

    public void setJoueurWs(JoueurWsDTO joueurWs) {
	this.joueurWs = joueurWs;
    }

    public void refresh() {
	this.listeUsers = this.userManagerFacade.findAllUsers();

	for (UserPOJO user : this.listeUsers) {
	    user.setMdp(fr.max.lvdc.core.Utils.encodeBCrypt(user.getMdp()));
	    this.userManagerFacade.saveMonCompte(user);
	}
    }

    public void saveComptes() {
	try {
	    for (JoueurPOJO joueur : selectedJoueurs) {
		this.user = new UserPOJO();

		RolePOJO role = new RolePOJO();
		role = this.userManagerFacade.getRole(2);
		this.user.setNom(joueur.getLicenceJoueur());
		this.user.setMdp(fr.max.lvdc.core.Utils.encodeBCrypt("LVDC"));
		this.user.setJoueur(joueur);
		joueur.setExterne(false);

		this.userManagerFacade.saveMonCompte(this.user);

	    }

	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage("Creation des comptes r�ussi", " "));

	} catch (Exception e) {
	    e.printStackTrace();
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Echec sauvegarde", " "));
	    
	}
    }

    public void saveJoueur() {
	if (joueurWs != null) {
	    ClubPOJO club = new ClubPOJO();
	    FiltreClubDTO filtre = new FiltreClubDTO();
	    filtre.setAbrevClub(joueurWs.getClub_court());
	    try {
		club = this.communFacade.findOneClubByFiltre(filtre);
	    } catch (Exception e) {
		club = new ClubPOJO();
		club.setAbrevClub(joueurWs.getClub_court());
		club.setNomClub(joueurWs.getClub());
		this.communFacade.saveClub(club);
	    }

	    JoueurPOJO joueur = new JoueurPOJO();

	    joueur.setNomJoueur(joueurWs.getNom());
	    joueur.setAgeJoueur(20);

	    joueur.setSexeJoueur(joueurWs.getSexe());

	    joueur.setClub(club);
	    joueur.setLicenceJoueur(joueurWs.getLicence());
	    joueur.setExterne(true);

	    joueur.setClassementSimple(joueurWs.getClassement_simple());
	    joueur.setClassementDouble(joueurWs.getClassement_double());
	    joueur.setClassementMixte(joueurWs.getClassement_mixte());

	    joueur.setCoteSimple(
		    (int) Float.parseFloat(joueurWs.getCote_simple()));
	    joueur.setCoteDouble(
		    (int) Float.parseFloat(joueurWs.getCote_double()));
	    joueur.setCoteMixte(
		    (int) Float.parseFloat(joueurWs.getCote_mixte()));

	    if (joueur.getClassementSimple().contains("P")
		    || joueur.getClassementDouble().contains("P")
		    || joueur.getClassementMixte().contains("P")) {
		joueur.setTourGardoisPaye(3);
		if (joueur.getClassementSimple().contains("D")
			|| joueur.getClassementDouble().contains("D")
			|| joueur.getClassementMixte().contains("D")) {
		    joueur.setTournoiPaye(1);
		    joueur.setTourGardoisPaye(0);

		    if (joueur.getClassementSimple().contains("R")
			    || joueur.getClassementDouble().contains("R")
			    || joueur.getClassementMixte().contains("R")) {
			joueur.setTournoiPaye(3);
			joueur.setTourGardoisPaye(0);

			if (joueur.getClassementSimple().contains("N")
				|| joueur.getClassementDouble().contains("N")
				|| joueur.getClassementMixte()
					.contains("N")) {
			    joueur.setTournoiPaye(5);
			    joueur.setTourGardoisPaye(0);
			}

		    }

		}
	    }

	    List<JoueurPOJO> listeJoueurs = this.communFacade.findAllJoueur();

	    boolean exist = false;

	    for (JoueurPOJO j : listeJoueurs) {
		if (j.getLicenceJoueur() != null) {
		    if (j.getLicenceJoueur()
			    .equals(joueur.getLicenceJoueur())) {
			exist = true;
		    }
		}
	    }

	    if (exist == false) {
		this.communFacade.saveJoueur(joueur);

		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage("Sauvegarde effectu�e du joueur "
				+ joueur.getNomJoueur()));
	    } else {
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage("Le joueur existe deja" + " "));
	    }

	    this.joueurWs = null;

	}

    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowSelect(final SelectEvent event) {

	this.selectedUser = ((UserPOJO) event.getObject());

    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowUnselect(final UnselectEvent event) {

	this.selectedUser = null;

    }

    public List<JoueurWsDTO> getListeJoueursAutocomplete() {
	return listeJoueursAutocomplete;
    }

    public List<JoueurWsDTO> rechercheExterne(String query) {
	this.listeJoueursAutocomplete = new ArrayList<>();
	this.listeJoueursAutocomplete =
		this.communFacade.rechercheListeExterne(query);
	return listeJoueursAutocomplete;
    }
}

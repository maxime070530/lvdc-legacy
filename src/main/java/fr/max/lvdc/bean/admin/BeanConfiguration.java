
package fr.max.lvdc.bean.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.dto.muduleinscription.StatusImportDTO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IImportFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@Component("beanConfiguration")
@Scope("view")
@Lazy(true)
public class BeanConfiguration {

    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userManagerFacade;

    @Autowired
    private IImportFacade importFacade;

    private StreamedContent fileExport;
    private InputStream fileJoueurs;
    private String nameFileDonnees;
    private StatusImportDTO status = null;
    private boolean toSaveReady;
    private boolean importEnCours = false;
    private List<UserPOJO> listeUsersToSave = new ArrayList<>();
    private List<List<String>> listeAnomalies;
    private String messageFin;
    private boolean isEnCourSave = false;

    @PostConstruct
    public void init() {
	toSaveReady = false;
    }

    public String getMessageFin() {
	return messageFin;
    }

    public void setMessageFin(String messageFin) {
	this.messageFin = messageFin;
    }

    public boolean isEnCourSave() {
	return isEnCourSave;
    }

    public void setEnCourSave(boolean isEnCourSave) {
	this.isEnCourSave = isEnCourSave;
    }

    public String getNameFileDonnees() {
	return nameFileDonnees;
    }

    public void setNameFileDonnees(String nameFileDonnees) {
	this.nameFileDonnees = nameFileDonnees;
    }

    public InputStream getFileJoueurs() {
	return fileJoueurs;
    }

    public void setFileJoueurs(InputStream fileJoueurs) {
	this.fileJoueurs = fileJoueurs;
    }

    public List<List<String>> getListeAnomalies() {
	return listeAnomalies;
    }

    public void setListeAnomalies(List<List<String>> listeAnomalies) {
	this.listeAnomalies = listeAnomalies;
    }

    public StatusImportDTO getStatus() {
	return status;
    }

    public void setStatus(StatusImportDTO status) {
	this.status = status;
    }

    public boolean isToSaveReady() {
	return toSaveReady;
    }

    public void setToSaveReady(boolean toSaveReady) {
	this.toSaveReady = toSaveReady;
    }

    public boolean isImportEnCours() {
	return importEnCours;
    }

    public void setImportEnCours(boolean importEnCours) {
	this.importEnCours = importEnCours;
    }

    public List<UserPOJO> getListeUsersToSave() {
	return listeUsersToSave;
    }

    public void setListeUsersToSave(List<UserPOJO> listeUsersToSave) {
	this.listeUsersToSave = listeUsersToSave;
    }

    public StreamedContent getFileExport() {
	return fileExport;
    }

    public void setFileExport(StreamedContent fileExport) {
	this.fileExport = fileExport;
    }

    public File exportDonnees() throws IOException {

	File temp = File.createTempFile("export", ".xlsx");

	final XSSFWorkbook book = new XSSFWorkbook();
	final Sheet sheet = book.createSheet("Export");
	for (int x = 0; x < 9; x++) {
	    sheet.setColumnWidth((short) x, (short) (20 * 256));
	    // sheet.autoSizeColumn(x, true);
	}
	int idxRow = 0;

	Row row = sheet.createRow(idxRow++);

	Cell cell = null;

	int idx = 0;

	cell = row.createCell(idx++);
	cell.setCellValue("Numero Licence");

	final FileOutputStream os = new FileOutputStream(temp);
	book.write(os);

	// Close workbook, OutputStream and Excel file to prevent leak
	os.close();
	book.close();

	return temp;

    }

    public void exportDonneesDownload() throws IOException {

	final File export = this.exportDonnees();

	final InputStream stream =
		com.google.common.io.Files.asByteSource(export).openStream();
	this.fileExport = new DefaultStreamedContent(stream,
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
		"export_donnees.xlsx");

    }

    public void importTourneeHisto() {

	try {

	    this.status = new StatusImportDTO();

	    importEnCours = true;

	    listeAnomalies = new ArrayList<List<String>>();
	    listeUsersToSave = new ArrayList<>();

	    toSaveReady = this.importFacade.importUser(fileJoueurs,
		    listeAnomalies, listeUsersToSave, this.status);

	} catch (Exception e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Veuillez uploader un fichier excel", " "));

	} finally {
	    importEnCours = false;
	    this.status = null;
	}

    }

    public void handleFileUpload(final FileUploadEvent event) {

	try {
	    fileJoueurs = event.getFile().getInputstream();
	    this.nameFileDonnees = event.getFile().getFileName();

	    final FacesMessage message =
		    new FacesMessage("Chargement du fichi� effectu� : "
			    + event.getFile().getFileName(), "");
	    FacesContext.getCurrentInstance().addMessage(null, message);
	} catch (final IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    final FacesMessage message =
		    new FacesMessage("Erreur : ", e.getMessage());
	    FacesContext.getCurrentInstance().addMessage(null, message);
	}

    }

    public void saveDonnee() {

	messageFin = "";

	try {
	    for (UserPOJO user : this.listeUsersToSave) {
		this.userManagerFacade.saveMonCompte(user);
		this.communFacade.saveJoueur(user.getJoueur());
	    }

	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage("import r�ussi", " "));
	    this.toSaveReady = false;

	} catch (final Exception e) {
	    e.printStackTrace();
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Echec import", " verifiez le squelette "));
	}

    }

    public void annuler() {
	this.toSaveReady = false;
	messageFin = "";
    }

}

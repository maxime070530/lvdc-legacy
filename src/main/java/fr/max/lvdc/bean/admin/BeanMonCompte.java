
package fr.max.lvdc.bean.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.CloseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@Component("beanMonCompte")
@Scope("view")
@Lazy(true)
public class BeanMonCompte extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The Constant LOGGER. */
    @SuppressWarnings(value = { "unused" })
    private static final Logger LOGGER =
	    Logger.getLogger(BeanMonCompte.class);

    // DEBUT INJECTION.

    @Autowired
    private IUserManagerFacade userManagerFacade;
    @Autowired
    private ICommunFacade communFacade;
    // FIN INJECTION.

    private UserPOJO utilisateur;
    private List<ClubPOJO> listeClubs = null;
    private List<JoueurPOJO> listeJoueurs = null;
    private JoueurPOJO joueur = null;
    private String newMdp;
    private String verifNewMdp;
    private String oldMdp;

    /**
     * 
     */
    @PostConstruct
    public void init() {
	this.listeClubs = this.communFacade.findAllClub();
	this.utilisateur = this.userManagerFacade
		.findUserByCode(getUserAuthenticate().getUsername(), true);
	this.listeJoueurs = this.communFacade.findAllJoueur();

	//
	// ConfigurableNavigationHandler nav =
	// (ConfigurableNavigationHandler) FacesContext
	// .getCurrentInstance().getApplication()
	// .getNavigationHandler();
	// nav.
	//
	// nav.getNavigationCase(FacesContext.getCurrentInstance(), null,
	// "MON_COMPTE").getToViewId(FacesContext.getCurrentInstance());

	FacesContext.getCurrentInstance().getViewRoot().getViewId();

    }

    /**
     * @return the utilisateur
     */
    public UserPOJO getUtilisateur() {
	return utilisateur;
    }

    public List<JoueurPOJO> getListeJoueurs() {
	return listeJoueurs;
    }

    public JoueurPOJO getJoueur() {
	return joueur;
    }

    public void setJoueur(JoueurPOJO joueur) {
	this.joueur = joueur;
    }

    public String getNewMdp() {
	return newMdp;
    }

    public void setNewMdp(String newMdp) {
	this.newMdp = newMdp;
    }

    public String getOldMdp() {
	return oldMdp;
    }

    public void setOldMdp(String oldMdp) {
	this.oldMdp = oldMdp;
    }

    public String getVerifNewMdp() {
	return verifNewMdp;
    }

    public void setVerifNewMdp(String verifNewMdp) {
	this.verifNewMdp = verifNewMdp;
    }

    /**
     * @param utilisateur
     *            the utilisateur to set
     */
    public void setUtilisateur(final UserPOJO utilisateur) {
	this.utilisateur = utilisateur;
    }

    public void annuler() {

	this.utilisateur = this.userManagerFacade
		.findUserByCode(getUserAuthenticate().getUsername(), true);
	setModificationGeneral(false);
    }

    public void saveMonCompte() {

	List<JoueurPOJO> listeJoueurs = new ArrayList<JoueurPOJO>();
	listeJoueurs.add(this.utilisateur.getJoueur());

	// this.communFacade.saveJoueur(this.utilisateur.getJoueur());
	this.communFacade.refreshClassement(listeJoueurs);

	// this.utilisateur.setMdp(fr.max.lvdc.core.Utils.encodeBCrypt(this.mdp));
	this.userManagerFacade.saveMonCompte(this.utilisateur);
	setModificationGeneral(false);

	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage("Sauvegarde effectu�e de l'utilisateur : "
			+ this.utilisateur.getNom(), " "));

    }

    public void refreshClassements() {
	this.communFacade.refreshClassement(this.listeJoueurs);

	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage("Les classements ont �t� mis � jour", " "));
    }

    public List<ClubPOJO> getListeClubs() {
	return listeClubs;
    }

    public void setListeClubs(List<ClubPOJO> listeClubs) {
	this.listeClubs = listeClubs;
    }

    // public void findJoueur(JoueurPOJO joueur) {
    //
    // this.utilisateur.setJoueur(joueur);
    // this.utilisateur = this.userManagerFacade.findUserByCode(
    // getUserAuthenticate().getUsername(), true);
    // }

    public void refreshJoueurs() {
	for (JoueurPOJO joueur : listeJoueurs) {
	    this.communFacade.saveJoueur(joueur);
	}
    }

    public void createAllCompte() {
	RolePOJO role = new RolePOJO();

	role = this.userManagerFacade.getRole(2);

	for (JoueurPOJO joueur : listeJoueurs) {
	    UserPOJO user = new UserPOJO();
	    user.setMdp(joueur.getNomJoueur());
	    user.setJoueur(joueur);
	    user.getListeRoles().add(role);
	    this.userManagerFacade.saveMonCompte(user);
	}

    }

    public void changeMdp() {

	if (fr.max.lvdc.core.Utils.matchBCrypt(this.oldMdp,
		this.utilisateur.getMdp())) {

	    this.utilisateur
		    .setMdp(fr.max.lvdc.core.Utils.encodeBCrypt(newMdp));
	    this.userManagerFacade.saveMonCompte(this.utilisateur);
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage("Changement de mot de passe r�ussi",
			    " "));

	} else {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "L'ancien mot de passe ne correspond pas", " "));
	}
    }

    public void initDialogMdp(final CloseEvent event) {

	this.oldMdp = null;
	this.newMdp = null;
	this.verifNewMdp = null;

	resetInputValues(event.getComponent().getChildren());

    }

    /**
     * @param event
     *            AjaxBehaviorEvent
     */
    public void openDialogMdp(final AjaxBehaviorEvent event) {

	this.oldMdp = null;
	this.newMdp = null;
	this.verifNewMdp = null;

    }

}

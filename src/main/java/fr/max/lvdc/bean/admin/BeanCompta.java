
package fr.max.lvdc.bean.admin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.io.Files;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IExportFacade;

@Component("beanCompta")
@Scope("view")
@Lazy(true)
public class BeanCompta extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IExportFacade exportFacade;

    private List<JoueurPOJO> listeJoueurs;

    private StreamedContent fileExport;

    private List<JoueurPOJO> selectedJoueurs = new ArrayList<>();

    private FiltreJoueurDTO filtre = new FiltreJoueurDTO();

    @PostConstruct
    public void init() {

	// this.listeJoueurs = this.communFacade.findJoueurByFiltre(filtre);
	filtre.setInterne(true);
	filtre.setConpta(true);
	rechercher();
    }

    public StreamedContent getFileExport() {
	return fileExport;
    }

    public void setFileExport(StreamedContent fileExport) {
	this.fileExport = fileExport;
    }

    public List<JoueurPOJO> getListeJoueurs() {
	return listeJoueurs;
    }

    public List<JoueurPOJO> getSelectedJoueurs() {
	return selectedJoueurs;
    }

    public void setSelectedJoueurs(List<JoueurPOJO> selectedJoueurs) {
	this.selectedJoueurs = selectedJoueurs;
    }

    public FiltreJoueurDTO getFiltre() {
	return filtre;
    }

    public void setFiltre(FiltreJoueurDTO filtre) {
	this.filtre = filtre;
    }

    public void exporter() throws IOException {

	final File export =
		this.exportFacade.exportCompta(this.selectedJoueurs);

	final InputStream stream = Files.asByteSource(export).openStream();
	this.fileExport = new DefaultStreamedContent(stream,
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
		"export_compta_" + new Date() + ".xlsx");

    }

    /**
     * 
     */
    public void rechercher() {

	this.listeJoueurs = this.communFacade.findJoueurByFiltre(filtre);

    }

}


package fr.max.lvdc.bean.admin;

import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreInscriptionDTO;
import fr.max.lvdc.facade.api.ICommunFacade;

@Component("beanListeInscriptionsCompta")
@Scope("view")
@Lazy(true)
public class BeanListeInscriptionsCompta extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private ICommunFacade communFacade;

    private List<InscriptionPOJO> listeInscriptions;

    private InscriptionPOJO selectedInscription;

    private FiltreInscriptionDTO filtre = new FiltreInscriptionDTO();

    @PostConstruct
    public void init() {
	filtre.setInterne(true);
	rechercher();
    }

    public List<InscriptionPOJO> getListeInscriptions() {
	return listeInscriptions;
    }

    public InscriptionPOJO getSelectedInscription() {
	return selectedInscription;
    }

    public void setSelectedInscription(InscriptionPOJO selectedInscription) {
	this.selectedInscription = selectedInscription;
    }

    public FiltreInscriptionDTO getFiltre() {
	return filtre;
    }

    public void setFiltre(FiltreInscriptionDTO filtre) {
	this.filtre = filtre;
    }

    public void save() {
	for (InscriptionPOJO i : this.listeInscriptions) {
	    this.communFacade.saveOrUpdateInscription(i);
	}

	setModificationGeneral(false);
    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowSelect(final SelectEvent event) {

	this.selectedInscription = ((InscriptionPOJO) event.getObject());

    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowUnselect(final UnselectEvent event) {

	this.selectedInscription = null;

    }

    /**
     * 
     */
    public void rechercher() {
	this.listeInscriptions =
		this.communFacade.findAllInscriptionsByFiltre(filtre);
    }

}

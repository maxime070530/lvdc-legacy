package fr.max.lvdc.bean.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;

import fr.max.lvdc.facade.api.ICommunFacade;

@FacesConverter("clubConverter")
public class ClubConverter implements Converter {

	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic,
			final String value) {

		if (value != null && StringUtils.isNumeric(value)) {

			@SuppressWarnings("unchecked")
			final List<ClubPOJO> liste = (List<ClubPOJO>) uic.getAttributes()
					.get("listeClubs");

			// la liste peut etre null si il refont pas l'autocomplete donc
			// findJoueur plus bas !
			if (liste != null) {
				for (final ClubPOJO current : liste) {
					if (current.getId().equals(Integer.valueOf(value))) {
						return current;
					}
				}
			}

			// ici faire plutot un find en base sur id ! pour gerer le cas du
			// lien sur une localite archive qui remonte pas dans listeLocalites
			// !

			final ApplicationContext ctx = FacesContextUtils
					.getWebApplicationContext(FacesContext.getCurrentInstance());
			final ICommunFacade communFacade = (ICommunFacade) ctx
					.getBean("communFacade");

			return communFacade.findClub(Integer.valueOf(value));
		}

		return null;
	}

	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic,
			final Object object) {
		if (object != null && object instanceof ClubPOJO) {
			return String.valueOf(((ClubPOJO) object).getId());
		}

		return null;

	}
}

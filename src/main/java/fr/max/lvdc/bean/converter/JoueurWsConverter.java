package fr.max.lvdc.bean.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import fr.max.lvdc.dto.ws.JoueurWsDTO;

@FacesConverter("joueurWsConverter")
public class JoueurWsConverter implements Converter {

	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {

		if (value != null) {

			@SuppressWarnings("unchecked")
			final List<JoueurWsDTO> liste = (List<JoueurWsDTO>) uic.getAttributes().get("listeJoueurWs");
			// la liste peut etre null si il refont pas l'autocomplete donc
			// findJoueur plus bas !
			if (liste != null) {
				for (final JoueurWsDTO current : liste) {
					if (current.getNom().equals(value)) {
						return current;
					}
				}
			}
		}
		return null;

	}

	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object != null && object instanceof JoueurWsDTO) {
			return String.valueOf(((JoueurWsDTO) object).getNom());
		}

		return null;

	}
}

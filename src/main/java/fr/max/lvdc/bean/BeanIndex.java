
package fr.max.lvdc.bean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserAuthenticate;
import fr.max.lvdc.facade.api.ICommunFacade;

@Component("beanIndex")
@Scope("view")
@Lazy(true)
public class BeanIndex extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 4072742364411257240L;
    @SuppressWarnings("unused")
    /** The Constant LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(BeanIndex.class);

    // debut injection
    @Autowired
    private ICommunFacade communFacade;

    private List<TournoiPOJO> listetournois = null;
    // fin injection
    private TournoiPOJO selectedTournoi = null;

    private Integer amount = 5;
    

    @PostConstruct
    public void init() {
	this.listetournois = this.communFacade.findAllTournoiByDate();

	Calendar cal = Calendar.getInstance();
	cal.setTime(new Date());
	if (cal.get(Calendar.DAY_OF_WEEK) == 5) {
	    if (getUserAuthenticate().getJoueur() != null) {
		List<JoueurPOJO> listeJoueurs = new ArrayList<>();
		listeJoueurs.add(getUserAuthenticate().getJoueur());
		this.communFacade.refreshClassement(listeJoueurs);
	    }
	}

    }

    public List<TournoiPOJO> getListetournois() {
	return listetournois;
    }

    public void setListetournois(List<TournoiPOJO> listetournois) {
	this.listetournois = listetournois;
    }

    public TournoiPOJO getSelectedTournoi() {
	return selectedTournoi;
    }

    public void setSelectedTournoi(TournoiPOJO selectedTournoi) {
	this.selectedTournoi = selectedTournoi;
    }

    public Integer getAmount() {
	return amount;
    }

    public void setAmount(Integer amount) {
	this.amount = amount;
    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowSelect(final SelectEvent event) {

	this.selectedTournoi = ((TournoiPOJO) event.getObject());

    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowUnselect(final UnselectEvent event) {

	this.selectedTournoi = null;

    }

    public void verif() {
	System.out.println("test");
    }

}

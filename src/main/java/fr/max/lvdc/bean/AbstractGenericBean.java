/**
 * 
 */

package fr.max.lvdc.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import fr.max.lvdc.core.Utils;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserAuthenticate;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.facade.api.ICommunFacade;

/**
 * @author mgrau
 */
public abstract class AbstractGenericBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    // DEBUT INJECTION.

    @Autowired
    private ICommunFacade communFacade;

    // FIN INJECTION.

    private boolean modificationGeneral = false;

    public boolean isModificationGeneral() {
	return modificationGeneral;
    }

    public void setModificationGeneral(boolean modificationGeneral) {
	this.modificationGeneral = modificationGeneral;
    }

    // modification infos joueur
    /**
    * 
    */
    public void startModification() {
	this.modificationGeneral = true;
    }

    /**
    * 
    */
    public void endModification() {
	this.modificationGeneral = false;
    }

    /**
     * @return String getLibelleHeader
     */
    public String getLibelleHeader() {

	return this.getLibelleHeader("", "");

    }

    /**
     * @param name
     *            String
     * @param libelle
     *            String
     * @return String getLibelleHeader
     */
    public String getLibelleHeader(final String name, final String libelle) {

	if (libelle == null || libelle.trim().isEmpty()) {
	    return "Cr�ation " + name;
	} else if (isModificationGeneral()) {
	    return "Modification " + name + " : " + libelle;
	} else {
	    return "Visualisation " + name + " : " + libelle;
	}

    }

    /**
     * @return String styleClass
     */
    public String getClassFieldSet() {

	if (this.isModificationGeneral()) {
	    return "fieldSet_Modify";
	}
	return "";
    }

    protected void resetInputValues(final List<UIComponent> children) {
	for (UIComponent component : children) {
	    if (component.getChildCount() > 0) {
		resetInputValues(component.getChildren());
	    } else {
		if (component instanceof EditableValueHolder) {
		    EditableValueHolder input =
			    (EditableValueHolder) component;
		    input.resetValue();
		}
	    }
	}
    }

    /**
     * @param query
     *            String
     * @return List<LocalitePOJO> autoCompleteVille
     */
    protected List<JoueurPOJO>
	    autoCompleteJoueurAbstract(final String query) {

	FiltreJoueurDTO filtreJoueurDTO = new FiltreJoueurDTO();
	filtreJoueurDTO.setNomJoueur(query);

	return this.communFacade.findJoueurByFiltre(filtreJoueurDTO);
    }

    /**
     * @param query
     *            String
     * @return List<LocalitePOJO> autoCompleteVille
     */
    protected List<ClubPOJO> autoCompleteClubAbstract(final String query) {

	FiltreClubDTO filtre = new FiltreClubDTO();
	filtre.setNomClub(query);

	return this.communFacade.findClubByFiltre(filtre);
    }

    /**
     * @return UserAuthenticate getUserAuthenticate
     */
    public UserAuthenticate getUserAuthenticate() {
	final Authentication authentication =
		SecurityContextHolder.getContext().getAuthentication();

	if (authentication != null) {
	    return (UserAuthenticate) authentication.getPrincipal();
	}

	return null;
    }

}

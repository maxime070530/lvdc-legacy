
package fr.max.lvdc.bean.moduleInscription;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.core.GetParameterUrl;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.dto.ws.JoueurWsDTO;
import fr.max.lvdc.exception.ValidationException;
import fr.max.lvdc.facade.api.ICommunFacade;

@Component("beanInscription")
@Scope("view")
@Lazy(true)
public class BeanInscription extends AbstractGenericBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5182682687333827226L;

	@Autowired
	private ICommunFacade communFacade;

	private InscriptionPOJO inscription;
	private TournoiPOJO tournoi;

	@GetParameterUrl(paramName = "idTournoi")
	private Integer idTournoi;

	@GetParameterUrl(paramName = "idInscription")
	private Integer idInscription;

	private List<JoueurPOJO> listeJoueurs;
	private List<JoueurWsDTO> listeJoueursAutoComplete;
	private List<JoueurWsDTO> listeJoueursAutoCompleteMixte;

	private JoueurWsDTO selectedJoueurWsDouble;
	private JoueurWsDTO selectedJoueurWsMixte;
	private Boolean edit = false;

	@PostConstruct
	public void init() {

		// RECHERCHE JOUEUR POUR ADMIN
		FiltreJoueurDTO filtre = new FiltreJoueurDTO();
		filtre.setInterne(true);
		this.listeJoueurs = this.communFacade.findJoueurByFiltre(filtre);

		this.tournoi = this.communFacade.findTournoi(this.idTournoi);

		if (idInscription != null) {
			this.edit = true;
			this.inscription = this.communFacade.findInscription(idInscription);
		} else {
			this.inscription = new InscriptionPOJO();
			this.inscription.setTournoi(this.tournoi);

			if (!getUserAuthenticate().getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMI"))) {
				this.inscription.setJoueur(getUserAuthenticate().getJoueur());
			}

			setModificationGeneral(true);
		}

	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public List<JoueurWsDTO> getListeJoueursAutoCompleteMixte() {
		return listeJoueursAutoCompleteMixte;
	}

	public List<JoueurWsDTO> getListeJoueursAutoComplete() {
		return listeJoueursAutoComplete;
	}

	public InscriptionPOJO getInscription() {
		return inscription;
	}

	public void setInscription(InscriptionPOJO inscription) {
		this.inscription = inscription;
	}

	public TournoiPOJO getTournoi() {
		return tournoi;
	}

	public void setTournoi(TournoiPOJO tournoi) {
		this.tournoi = tournoi;
	}

	public List<JoueurPOJO> getListeJoueurs() {
		return listeJoueurs;
	}

	public JoueurWsDTO getSelectedJoueurWsDouble() {
		return selectedJoueurWsDouble;
	}

	public void setSelectedJoueurWsDouble(JoueurWsDTO selectedJoueurWsDouble) {
		this.selectedJoueurWsDouble = selectedJoueurWsDouble;
	}

	public JoueurWsDTO getSelectedJoueurWsMixte() {
		return selectedJoueurWsMixte;
	}

	public void setSelectedJoueurWsMixte(JoueurWsDTO selectedJoueurWsMixte) {
		this.selectedJoueurWsMixte = selectedJoueurWsMixte;
	}

	public String getPlaceHolder() {
		if (this.inscription.getPartenaireDouble() != null) {
			return this.inscription.getPartenaireDouble().getNomJoueur() + "     Saisir un autre nom pour changer";
		}
		return "Nom / Prenom / Licence";
	}

	public String getPlaceHolderMixte() {
		if (this.inscription.getPartenaireMixte() != null) {
			return this.inscription.getPartenaireMixte().getNomJoueur() + "     Saisir un autre nom pour changer";
		}
		return "Nom / Prenom / Licence";
	}

	@Override
	public String getLibelleHeader() {
		if (idInscription != null) {
			return super.getLibelleHeader("Inscription de", this.inscription.getJoueur().getLabel());
		} else {
			return super.getLibelleHeader("Inscription", "");
		}
	}

	public void annuler() {
		setModificationGeneral(false);
		this.inscription = this.communFacade.findInscription(this.inscription.getId());
	}

	public String delete() {

		try {
			if (this.inscription.isPaye() && !this.inscription.getTournoi().isTourGardois()) {
				this.inscription.getJoueur().setTournoiPaye(this.inscription.getJoueur().getTournoiPaye() + 1);
				this.communFacade.saveJoueur(this.inscription.getJoueur());
			}
			this.communFacade.deleteInscription(this.inscription);

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Suppression de l'inscription reussi " + ""));
			return "/jsf/tournois/fichetournoi.xhtml?true&includeViewParams=true";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<JoueurWsDTO> rechercheExterne(String query) {
		this.listeJoueursAutoComplete = new ArrayList<>();
		this.listeJoueursAutoComplete = this.communFacade.rechercheListeExterne(query);
		return listeJoueursAutoComplete;
	}

	public List<JoueurWsDTO> rechercheExterneMixte(String query) {
		this.listeJoueursAutoCompleteMixte = new ArrayList<>();
		this.listeJoueursAutoCompleteMixte = this.communFacade.rechercheListeExterne(query);
		return listeJoueursAutoCompleteMixte;
	}

	public void save() {

		try {
			this.inscription.setDateInscription(new Date());

			// GESTION PARTENAIRE
			if (this.selectedJoueurWsDouble != null) {

				List<JoueurPOJO> listeJoueurs = this.communFacade.findAllJoueur();

				for (JoueurPOJO joueur : listeJoueurs) {
					if (this.selectedJoueurWsDouble.getLicence().equals(joueur.getLicenceJoueur())) {
						this.inscription.setPartenaireDouble(joueur);
					}
				}

				if (this.inscription.getPartenaireDouble() == null) {
					this.inscription.setPartenaireDouble(this.communFacade.saveJoueurWs(selectedJoueurWsDouble));
				}
			}

			if (this.selectedJoueurWsMixte != null) {

				List<JoueurPOJO> listeJoueurs = this.communFacade.findAllJoueur();

				for (JoueurPOJO joueur : listeJoueurs) {
					if (this.selectedJoueurWsMixte.getLicence().equals(joueur.getLicenceJoueur())) {
						this.inscription.setPartenaireMixte(joueur);
					}
				}

				if (this.inscription.getPartenaireMixte() == null) {

					this.inscription.setPartenaireMixte(this.communFacade.saveJoueurWs(selectedJoueurWsMixte));
				}
			}

			this.selectedJoueurWsDouble = null;
			this.selectedJoueurWsMixte = null;
			// GESTION PARTENAIRE

			Integer prix = 0;

			prix = inscription.getTournoi().getPrixUnTableau();

			if (inscription.getTournoi().getPrixDeuxTableau() != null && inscription.getNbTableau() == 2) {
				prix = inscription.getTournoi().getPrixDeuxTableau();
			}

			if (inscription.getTournoi().getPrixTroisTableau() != null && inscription.getNbTableau() == 3) {
				prix = inscription.getTournoi().getPrixTroisTableau();
			}

			inscription.setPrix(prix);

			if (inscription.getJoueur().getTournoiPaye() != null
					|| inscription.getJoueur().getTourGardoisPaye() != null) {
				if (inscription.getJoueur().getTournoiPaye() != 0 && !inscription.getTournoi().isTourGardois()) {
					inscription.setPaye(true);
				}

				if (inscription.getJoueur().getTourGardoisPaye() != 0 && inscription.getTournoi().isTourGardois()) {
					inscription.setPaye(true);
				}

				if (inscription.getTournoi().isTourGardois() == true
						&& inscription.getJoueur().getTourGardoisPaye() != 0 && this.edit == false) {
					inscription.getJoueur().setTourGardoisPaye(inscription.getJoueur().getTourGardoisPaye() - 1);

				} else if (!inscription.getTournoi().isTourGardois() && inscription.getJoueur().getTournoiPaye() != 0
						&& this.edit == false) {
					inscription.getJoueur().setTournoiPaye(inscription.getJoueur().getTournoiPaye() - 1);
				}
			}

			this.communFacade.saveOrUpdateInscription(this.inscription);
			this.communFacade.saveJoueur(inscription.getJoueur());

			setModificationGeneral(false);

			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					"Sauvegarde effectu�e de l'inscription de " + this.inscription.getJoueur().getLabel(), ""));

		} catch (ValidationException ve) {

			final List<String> errors = ve.getFeedBack().getListMessages();
			if (!errors.isEmpty()) {

				for (final String current : errors) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, current, ""));
				}
			}
		}

	}

	public void editInscription() {
		setModificationGeneral(true);
		this.edit = true;
	}

	public void update() {
		try {
			if (this.selectedJoueurWsDouble != null) {

				List<JoueurPOJO> listeJoueurs = this.communFacade.findAllJoueur();

				for (JoueurPOJO joueur : listeJoueurs) {
					if (this.selectedJoueurWsDouble.getLicence().equals(joueur.getLicenceJoueur())) {
						this.inscription.setPartenaireDouble(joueur);
					}
				}

				if (this.inscription.getPartenaireDouble() == null) {
					this.inscription.setPartenaireDouble(this.communFacade.saveJoueurWs(selectedJoueurWsDouble));
				}
			}

			if (this.selectedJoueurWsMixte != null) {

				List<JoueurPOJO> listeJoueurs = this.communFacade.findAllJoueur();

				for (JoueurPOJO joueur : listeJoueurs) {
					if (this.selectedJoueurWsMixte.getLicence().equals(joueur.getLicenceJoueur())) {
						this.inscription.setPartenaireMixte(joueur);
					}
				}

				if (this.inscription.getPartenaireMixte() == null) {

					this.inscription.setPartenaireMixte(this.communFacade.saveJoueurWs(selectedJoueurWsMixte));
				}
			}

			this.selectedJoueurWsDouble = null;
			this.selectedJoueurWsMixte = null;
			// GESTION PARTENAIRE

			this.communFacade.saveOrUpdateInscription(this.inscription);
			setModificationGeneral(false);

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Les modifications ont �t� enregistr�es ", ""));

		} catch (

		ValidationException ve) {

			final List<String> errors = ve.getFeedBack().getListMessages();
			if (!errors.isEmpty()) {

				for (final String current : errors) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, current, ""));
				}
			}
		}

	}

}


package fr.max.lvdc.bean.moduleInscription;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.core.GetParameterUrl;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@Component("beanJoueur")
@Scope("view")
@Lazy(true)
public class BeanJoueur extends AbstractGenericBean {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = Logger.getLogger(BeanJoueur.class);

    /**
     * 
     */
    private static final long serialVersionUID = -8747766316855724730L;

    // injection
    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userManagerFacade;
    // injection

    private JoueurPOJO joueur = null;

    private UserPOJO user = new UserPOJO();

    @GetParameterUrl(paramName = "idJoueur")
    private Integer idJoueur;

    private List<ClubPOJO> listeClubs = null;
    private List<VeteranPOJO> listeClassementVeterans = null;

    @PostConstruct
    public void init() {

	this.listeClubs = this.communFacade.findAllClub();

	if (idJoueur != null) {
	    this.joueur = this.communFacade.findJoueur(idJoueur);
	    this.user =
		    this.userManagerFacade.findUserByJoueur(this.idJoueur);

	} else {
	    this.joueur = new JoueurPOJO();

	    setModificationGeneral(true);
	}
    }

    public List<VeteranPOJO> getListeClassementVeterans() {
	return listeClassementVeterans;
    }

    public JoueurPOJO getJoueur() {
	return joueur;
    }

    public void setJoueur(JoueurPOJO joueur) {
	this.joueur = joueur;
    }

    public List<ClubPOJO> getListeClubs() {
	return listeClubs;
    }

    public void setListeClubs(List<ClubPOJO> listeClubs) {
	this.listeClubs = listeClubs;
    }

    public UserPOJO getUser() {
	return user;
    }

    public void setUser(UserPOJO user) {
	this.user = user;
    }

    /**
     * 
     */
    public void save() {
	try {
	    List<JoueurPOJO> listeJoueurs = new ArrayList<JoueurPOJO>();
	    listeJoueurs.add(this.joueur);

	    this.communFacade.saveJoueur(this.joueur);
	    this.communFacade.refreshClassement(listeJoueurs);

	    setModificationGeneral(false);
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage("Sauvegarde effectu�e du joueur", " "));

	} catch (final Exception e) {
	    e.printStackTrace();
	    // message d erreur
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    e.getMessage(), " "));
	}

    }

    /**
     * 
     */
    public void annuler() {

	setModificationGeneral(false);
	this.joueur = this.communFacade.findJoueur(this.joueur.getId());

    }

    public String delete() {
	if (user != null) {
	    this.user.setJoueur(null);
	}
	this.communFacade.deleteJoueur(this.joueur);

	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage("Suppression du joueur ", " "));

	return "LISTE_JOUEURS";

    }

    @Override
    public String getLibelleHeader() {
	return super.getLibelleHeader("Joueur", this.joueur.getLabel());
    }

    public void createCompte() {

	RolePOJO role = new RolePOJO();
	role = this.userManagerFacade.getRole(2);
	user = new UserPOJO();
	this.user.setMdp(fr.max.lvdc.core.Utils
		.encodeBCrypt(this.joueur.getNomJoueur()));
	this.user.setJoueur(this.joueur);
	this.user.getListeRoles().add(role);
	this.userManagerFacade.saveMonCompte(this.user);

	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage("Creation du compte r�ussi", " "));
    }

    public void resetMdp() {

	this.user = this.userManagerFacade.findUserByJoueur(this.idJoueur);
	this.user.setMdp(fr.max.lvdc.core.Utils
		.encodeBCrypt(this.joueur.getClub().getAbrevClub()));
	this.userManagerFacade.saveMonCompte(this.user);
    }

}

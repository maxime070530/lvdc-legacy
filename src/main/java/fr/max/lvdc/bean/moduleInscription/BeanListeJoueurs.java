package fr.max.lvdc.bean.moduleInscription;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.facade.api.ICommunFacade;

@Component("beanListeJoueurs")
@Scope("view")
@Lazy(true)
public class BeanListeJoueurs extends AbstractGenericBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6108357109501329869L;
	
	@SuppressWarnings("unused")
	/** The Constant LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(BeanListeJoueurs.class);

	//debut injection
	@Autowired
	private ICommunFacade communFacade;
	//fin injection
	
	private List<JoueurPOJO> listejoueurs = null;
	private JoueurPOJO selectedJoueur = null;
	private FiltreJoueurDTO filtre = new FiltreJoueurDTO();
	

	@PostConstruct
	public void init(){
		this.listejoueurs = this.communFacade.findAllJoueur();
	}

	public List<JoueurPOJO> getListejoueurs() {
		return listejoueurs;
	}

	public void setListejoueurs(List<JoueurPOJO> listejoueurs) {
		this.listejoueurs = listejoueurs;
	}

	public JoueurPOJO getSelectedJoueur() {
		return selectedJoueur;
	}

	public void setSelectedJoueur(JoueurPOJO selectedJoueur) {
		this.selectedJoueur = selectedJoueur;
	}
	
	
	public FiltreJoueurDTO getFiltre() {
		return filtre;
	}

	public void setFiltre(FiltreJoueurDTO filtre) {
		this.filtre = filtre;
	}

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowSelect(final SelectEvent event) {

	this.selectedJoueur = ((JoueurPOJO) event.getObject());

    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowUnselect(final UnselectEvent event) {

	this.selectedJoueur = null;

    }
    
    /**
     * 
     */
    public void rechercher() {

	this.listejoueurs =
		this.communFacade
			.findJoueurByFiltre(filtre);
    }

}

package fr.max.lvdc.bean.moduleInscription;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.facade.api.ICommunFacade;

@Component("beanListeClubs")
@Scope("view")
@Lazy(true)
public class BeanListeClubs extends AbstractGenericBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8434755034376779001L;
	@SuppressWarnings("unused")
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(BeanListeClubs.class);

	@Autowired
	private ICommunFacade communFacade;

	private List<ClubPOJO> listeClubs = null;
	private ClubPOJO selectedClub = null;
	private FiltreClubDTO filtre = new FiltreClubDTO();

	@PostConstruct
	public void init() {
		this.listeClubs = this.communFacade.findAllClub();
	}

	public List<ClubPOJO> getListeClubs() {
		return listeClubs;
	}

	public void setListeClubs(List<ClubPOJO> listeClubs) {
		this.listeClubs = listeClubs;
	}

	public ClubPOJO getSelectedClub() {
		return selectedClub;
	}

	public void setSelectedClub(ClubPOJO selectedClub) {
		this.selectedClub = selectedClub;
	}
	
	

	/**
	 * @param event
	 *            SelectEvent
	 */
	public void onRowSelect(final SelectEvent event) {

		this.selectedClub = ((ClubPOJO) event.getObject());

	}

	/**
	 * @param event
	 *            SelectEvent
	 */
	public void onRowUnselect(final UnselectEvent event) {

		this.selectedClub = null;

	}

	public FiltreClubDTO getFiltre() {
		return filtre;
	}

	public void setFiltre(FiltreClubDTO filtre) {
		this.filtre = filtre;
	}

	/**
     * 
     */
	public void rechercher() {

		this.listeClubs = this.communFacade.findClubByFiltre(filtre);
	}

}
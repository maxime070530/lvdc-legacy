//
//package fr.max.lvdc.bean.moduleInscription;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//import javax.faces.application.FacesMessage;
//import javax.faces.context.FacesContext;
//import javax.faces.event.AjaxBehaviorEvent;
//
//import org.apache.log4j.Logger;
//import org.primefaces.event.CloseEvent;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.context.annotation.Scope;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.stereotype.Component;
//
//import fr.max.lvdc.bean.AbstractGenericBean;
//import fr.max.lvdc.core.GetParameterUrl;
//import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
//import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
//import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
//import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
//import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
//import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
//import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;
//import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
//import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
//import fr.max.lvdc.dto.ws.JoueurWsDTO;
//import fr.max.lvdc.facade.api.ICommunFacade;
//import fr.max.lvdc.facade.api.IUserManagerFacade;
//
//@Component("beanInscription")
//@Scope("view")
//@Lazy(true)
//public class BeanInscriptionLegacy extends AbstractGenericBean {
//
//    @SuppressWarnings("unused")
//    private static final Logger LOGGER =
//	    Logger.getLogger(BeanInscriptionLegacy.class);
//
//    /**
//     * 
//     */
//    private static final long serialVersionUID = -2315023401394978829L;
//
//    @Autowired
//    private ICommunFacade communFacade;
//
//    private InscriptionPOJO inscription = null;
//
//    @GetParameterUrl(paramName = "idTournoi")
//    private Integer idTournoi;
//
//    @GetParameterUrl(paramName = "idInscription")
//    private Integer idInscription;
//
//    private TournoiPOJO tournoi = null;
//
//    private List<JoueurPOJO> listeJoueurs = null;
//    private List<ClubPOJO> listeClubs = null;
//    private List<InscriptionPOJO> listeInscriptions = null;
//    private List<InscriptionPOJO> listeInscriptionsVeterans = null;
//    private List<JoueurPOJO> listeJoueursSansExterne = null;
//    private List<JoueurPOJO> listeVeterans = null;
//
//    private List<VeteranPOJO> listeCategorieVeterans = null;
//
//    private UserPOJO user = null;
//
//    private Boolean deleteInscr = false;
//    private RolePOJO role = null;
//
//    private JoueurWsDTO selectedJoueurWsDouble;
//    private JoueurWsDTO selectedJoueurWsMixte;
//
//    private List<JoueurWsDTO> listeJoueursAutocomplete;
//    private List<JoueurWsDTO> listeJoueursAutocompleteMixte;
//
//    private List<String> listeAnomalies = new ArrayList<>();
//
//    private Boolean edit = false;
//
//    private Boolean actifDouble = true;
//    private Boolean actifMixte = true;
//
//    @PostConstruct
//    public void init() {
//	this.listeClubs = this.communFacade.findAllClub();
//
//	FiltreJoueurDTO filtre = new FiltreJoueurDTO();
//	filtre.setInterne(true);
//
//	this.listeJoueurs = this.communFacade.findJoueurByFiltre(filtre);
//
//	this.tournoi = this.communFacade.findTournoi(idTournoi);
//
//	if (idInscription != null) {
//
//	    this.inscription =
//		    this.communFacade.findInscription(idInscription);
//	    setModificationGeneral(false);
//	    if (this.inscription.getJoueur()
//		    .equals(getUserAuthenticate().getJoueur())) {
//		deleteInscr = true;
//	    }
//
//	} else {
//	    this.inscription = new InscriptionPOJO();
//	    inscription.setTournoi(this.tournoi);
//
//	    if (!getUserAuthenticate().getAuthorities()
//		    .contains(new SimpleGrantedAuthority("ROLE_ADMI"))) {
//
//		this.inscription.setJoueur(getUserAuthenticate().getJoueur());
//		renderedCategorie();
//
//	    }
//
//	    setModificationGeneral(true);
//	    // externeDouble();
//	    // externeMixte();
//	}
//
//    }
//
//    public void renderedCategorie() {
//	for (InscriptionPOJO i : this.tournoi.getListeInscriptions()) {
//	    if (this.inscription.getJoueur()
//		    .equals(i.getPartenaireDouble())) {
//
//		actifDouble = false;
//	    }
//
//	    if (this.inscription.getJoueur().equals(i.getPartenaireMixte())) {
//
//		actifMixte = false;
//	    }
//	}
//    }
//
//    public Boolean getActifDouble() {
//	return actifDouble;
//    }
//
//    public void setActifDouble(Boolean actifDouble) {
//	this.actifDouble = actifDouble;
//    }
//
//    public Boolean getActifMixte() {
//	return actifMixte;
//    }
//
//    public void setActifMixte(Boolean actifMixte) {
//	this.actifMixte = actifMixte;
//    }
//
//    public Boolean getEdit() {
//	return edit;
//    }
//
//    public void setEdit(Boolean edit) {
//	this.edit = edit;
//    }
//
//    public List<JoueurWsDTO> getListeJoueursAutocompleteMixte() {
//	return listeJoueursAutocompleteMixte;
//    }
//
//    public Boolean getDeleteInscr() {
//	return deleteInscr;
//    }
//
//    public void setDeleteInscr(Boolean deleteInscr) {
//	this.deleteInscr = deleteInscr;
//    }
//
//    public RolePOJO getRole() {
//	return role;
//    }
//
//    public void setRole(RolePOJO role) {
//	this.role = role;
//    }
//
//    public InscriptionPOJO getInscription() {
//	return inscription;
//    }
//
//    public List<JoueurPOJO> getListeJoueursSansExterne() {
//	return listeJoueursSansExterne;
//    }
//
//    public void setInscription(InscriptionPOJO inscription) {
//	this.inscription = inscription;
//    }
//
//    public List<JoueurPOJO> getListeJoueurs() {
//	return listeJoueurs;
//    }
//
//    public List<ClubPOJO> getListeClubs() {
//	return listeClubs;
//    }
//
//    public TournoiPOJO getTournoi() {
//	return tournoi;
//    }
//
//    public void setTournoi(TournoiPOJO tournoi) {
//	this.tournoi = tournoi;
//    }
//
//    public List<InscriptionPOJO> getListeInscriptions() {
//	return listeInscriptions;
//    }
//
//    public List<JoueurPOJO> getListeVeterans() {
//	return listeVeterans;
//    }
//
//    public List<VeteranPOJO> getListeCategorieVeterans() {
//	return listeCategorieVeterans;
//    }
//
//    public List<InscriptionPOJO> getListeInscriptionsVeterans() {
//	return listeInscriptionsVeterans;
//    }
//
//    public UserPOJO getUser() {
//	return user;
//    }
//
//    public void setUser(UserPOJO user) {
//	this.user = user;
//    }
//
//    public List<JoueurWsDTO> getListeJoueursAutocomplete() {
//	return listeJoueursAutocomplete;
//    }
//
//    public JoueurWsDTO getSelectedJoueurWsDouble() {
//	return selectedJoueurWsDouble;
//    }
//
//    public void
//	    setSelectedJoueurWsDouble(JoueurWsDTO selectedJoueurWsDouble) {
//	this.selectedJoueurWsDouble = selectedJoueurWsDouble;
//    }
//
//    public JoueurWsDTO getSelectedJoueurWsMixte() {
//	return selectedJoueurWsMixte;
//    }
//
//    public void setSelectedJoueurWsMixte(JoueurWsDTO selectedJoueurWsMixte) {
//	this.selectedJoueurWsMixte = selectedJoueurWsMixte;
//    }
//
//    public List<String> getListeAnomalies() {
//	return listeAnomalies;
//    }
//
//    @Override
//    public String getLibelleHeader() {
//	if (idInscription != null) {
//	    return super.getLibelleHeader("Inscription de",
//		    this.inscription.getJoueur().getLabel());
//	} else {
//	    return super.getLibelleHeader("Inscription", "");
//	}
//    }
//
//    public String getPlaceHolder() {
//	if (this.inscription.getPartenaireDouble() != null) {
//	    return this.inscription.getPartenaireDouble().getNomJoueur()
//		    + "     Saisir un autre nom pour changer";
//	}
//	return "Nom / Prenom / Licence";
//    }
//
//    public String getPlaceHolderMixte() {
//	if (this.inscription.getPartenaireMixte() != null) {
//	    return this.inscription.getPartenaireMixte().getNomJoueur()
//		    + "     Saisir un autre nom pour changer";
//	}
//	return "Nom / Prenom / Licence";
//    }
//
//    public boolean getPoss() {
//	if (this.inscription != null) {
//	    if (this.inscription.getJoueur()
//		    .equals(getUserAuthenticate().getJoueur())) {
//		return true;
//	    }
//	}
//	return false;
//    }
//
//    // GESTION DES EXCEPTIONS
//    public Boolean checkAnomalies(InscriptionPOJO inscription) {
//	for (InscriptionPOJO i : this.tournoi.getListeInscriptions()) {
//
//	    if (i.getPartenaireDouble() != null) {
//		if (inscription.isCategorieDouble() && inscription.getJoueur()
//			.equals(i.getPartenaireDouble())) {
//		    String ano =
//			    "Vous etes d�j� inscrit dans le tableau double avec une autre personne";
//		    this.listeAnomalies.add(ano);
//		    return false;
//		}
//	    }
//
//	    if (i.getPartenaireMixte() != null) {
//		if (inscription.isCategorieMixte() && inscription.getJoueur()
//			.equals(i.getPartenaireMixte())) {
//		    String ano1 =
//			    "Vous etes d�j� inscrit dans le tableau mixte avec une autre personne";
//		    this.listeAnomalies.add(ano1);
//		    return false;
//
//		}
//	    }
//
//	    if (inscription.getJoueur().equals(i.getJoueur())
//		    && inscription.isCategorieDouble()
//		    && i.isCategorieDouble()) {
//		String ano4 = "Vous etes d�j� inscrit dans le tableau double";
//		this.listeAnomalies.add(ano4);
//		return false;
//	    }
//
//	    if (inscription.getJoueur().equals(i.getJoueur())
//		    && inscription.isCategorieMixte()
//		    && i.isCategorieMixte()) {
//		String ano4 = "Vous etes d�j� inscrit dans le tableau mixte";
//		this.listeAnomalies.add(ano4);
//		return false;
//	    }
//
//	    if (inscription.getPartenaireDouble() != null
//		    && i.getPartenaireDouble() != null) {
//		if (inscription.isCategorieDouble()
//			&& ((inscription.getPartenaireDouble()
//				.equals(i.getPartenaireDouble()))
//				|| (inscription.getPartenaireDouble()
//					.equals(i.getJoueur())))) {
//		    String ano2 =
//			    "Votre partenaire est d�j� inscrit dans ce tableau";
//		    this.listeAnomalies.add(ano2);
//		    return false;
//		}
//	    }
//
//	    if (inscription.getPartenaireMixte() != null
//		    && i.getPartenaireMixte() != null) {
//		if (inscription.isCategorieMixte() && ((inscription
//			.getPartenaireMixte().equals(i.getPartenaireMixte())
//			|| inscription.getPartenaireMixte()
//				.equals(i.getJoueur())))) {
//		    String ano3 =
//			    "Votre partenaire est d�j� inscrit dans ce tableau";
//		    this.listeAnomalies.add(ano3);
//		    return false;
//		}
//	    }
//
//	    if (inscription.isCategorieSimple()
//		    && inscription.getJoueur().equals(i.getJoueur())
//		    && i.isCategorieSimple()) {
//		String ano6 =
//			"Vous etes d�j� inscrit en simple sur ce tournoi";
//		this.listeAnomalies.add(ano6);
//		return false;
//	    }
//
//	    // if (inscription.getJoueur().equals(i.getJoueur()) && (i
//	    // .getNbTableau() > i.getTournoi().getNbTableau()
//	    // || i.getNbTableau() == i.getTournoi().getNbTableau())) {
//	    // String ano7 =
//	    // "Vous etes d�j� inscrit au nombre maximum ";
//	    // this.listeAnomalies.add(ano6);
//	    // return false;
//	    //
//	    // }
//
//	}
//
//	return true;
//
//    }
//
//    public Boolean checkExist(InscriptionPOJO inscription) {
//	for (InscriptionPOJO i : this.tournoi.getListeInscriptions()) {
//	    if (inscription.getJoueur().equals(i.getJoueur())) {
//		return false;
//	    }
//	}
//	return true;
//    }
//
//    public InscriptionPOJO
//	    equilibrageInscription(InscriptionPOJO inscription) {
//	for (InscriptionPOJO i : this.tournoi.getListeInscriptions()) {
//	    if (inscription.getJoueur().equals(i.getPartenaireDouble())) {
//		inscription.setCategorieDouble(true);
//		inscription.setPartenaireDouble(i.getJoueur());
//	    }
//
//	    if (inscription.getJoueur().equals(i.getPartenaireMixte())) {
//		inscription.setCategorieMixte(true);
//		inscription.setPartenaireMixte(i.getJoueur());
//	    }
//	}
//	return inscription;
//    }
//
//    // public void createInscription(InscriptionPOJO inscription) {
//    // for(InscriptionPOJO i: this.tournoi.getListeInscriptions()) {
//    // if(!inscription.getPartenaireDouble().equals(i.getJoueur())) {
//    // InscriptionPOJO inscr = new InscriptionPOJO();
//    // inscr.setJoueur(inscription.getPartenaireDouble());
//    // inscr.setCategorieDouble(true);
//    // inscr.setPartenaireDouble(i.getJoueur());
//    // }
//    // }
//    // }
//
//    public void save(Boolean paye) {
//
//	try {
//	    // DESCENDRE
//	    if (this.inscription.isCategorieSimple()
//		    || this.inscription.isCategorieDouble()
//		    || this.inscription.isCategorieMixte()) {
//
//		if ((this.inscription.getNbTableau() < this.tournoi
//			.getNbTableau())
//			|| (this.inscription.getNbTableau() == this.tournoi
//				.getNbTableau())) {
//
//		    this.inscription.setDateInscription(new Date());
//
//		    // GESTION PARTENAIRE
//		    if (this.selectedJoueurWsDouble != null) {
//
//			List<JoueurPOJO> listeJoueurs =
//				this.communFacade.findAllJoueur();
//
//			for (JoueurPOJO joueur : listeJoueurs) {
//			    if (this.selectedJoueurWsDouble.getLicence()
//				    .equals(joueur.getLicenceJoueur())) {
//				this.inscription.setPartenaireDouble(joueur);
//			    }
//			}
//
//			if (this.inscription.getPartenaireDouble() == null) {
//			    this.inscription.setPartenaireDouble(
//				    this.communFacade.saveJoueurWs(
//					    selectedJoueurWsDouble));
//			}
//		    }
//
//		    if (this.selectedJoueurWsMixte != null) {
//
//			List<JoueurPOJO> listeJoueurs =
//				this.communFacade.findAllJoueur();
//
//			for (JoueurPOJO joueur : listeJoueurs) {
//			    if (this.selectedJoueurWsMixte.getLicence()
//				    .equals(joueur.getLicenceJoueur())) {
//				this.inscription.setPartenaireMixte(joueur);
//			    }
//			}
//
//			if (this.inscription.getPartenaireMixte() == null) {
//
//			    this.inscription.setPartenaireMixte(
//				    this.communFacade.saveJoueurWs(
//					    selectedJoueurWsMixte));
//			}
//		    }
//
//		    this.selectedJoueurWsDouble = null;
//		    this.selectedJoueurWsMixte = null;
//		    // GESTION PARTENAIRE
//
//		    Integer prix = 0;
//
//		    prix = this.tournoi.getPrixUnTableau();
//
//		    if (this.tournoi.getPrixDeuxTableau() != null
//			    && this.inscription.getNbTableau() == 2) {
//			prix = this.tournoi.getPrixDeuxTableau();
//		    }
//
//		    if (this.tournoi.getPrixTroisTableau() != null
//			    && this.inscription.getNbTableau() == 3) {
//			prix = this.tournoi.getPrixTroisTableau();
//		    }
//
//		    this.inscription.setPrix(prix);
//
//		    if (checkAnomalies(this.inscription)
//			    && this.edit == false) {
//
//			// GESTION PRIX TOTAL D'UNE INSCRIPTION
//
//			// GESTION PRIX TOTAL D'UNE INSCRIPTION
//
//			// if (checkIfAlreadyInPair(inscription) ||
//			// (this.tournoi.getListeInscriptions().isEmpty()
//			// || this.tournoi.getListeInscriptions() == null))
//			// {
//
//			// PAYE OU NON
//			if (paye == false) {
//
//			    this.inscription.setPaye(false);
//			    this.inscription =
//				    equilibrageInscription(this.inscription);
//			    this.communFacade.saveOrUpdateInscription(
//				    this.inscription);
//
//			} else if (paye == true) {
//			    this.inscription.setPaye(true);
//
//			    if (this.tournoi.isTourGardois() == true
//				    && this.inscription.getJoueur()
//					    .getTourGardoisPaye() != 0) {
//				this.inscription.getJoueur()
//					.setTourGardoisPaye(
//						this.inscription.getJoueur()
//							.getTourGardoisPaye()
//							- 1);
//				this.communFacade.saveJoueur(
//					this.inscription.getJoueur());
//
//				FacesContext.getCurrentInstance().addMessage(
//					null,
//					new FacesMessage("Il vous reste "
//						+ this.inscription.getJoueur()
//							.getTourGardoisPaye()
//						+ " tour gardois pay�s",
//						" "));
//
//			    } else if (!this.tournoi.isTourGardois()
//				    && this.inscription.getJoueur()
//					    .getTournoiPaye() != 0) {
//				this.inscription.getJoueur()
//					.setTournoiPaye(this.inscription
//						.getJoueur().getTournoiPaye()
//						- 1);
//				this.communFacade.saveJoueur(
//					this.inscription.getJoueur());
//				FacesContext.getCurrentInstance().addMessage(
//					null,
//					new FacesMessage("Il vous reste "
//						+ this.inscription.getJoueur()
//							.getTournoiPaye()
//						+ " tournois pay�s par le club",
//						" "));
//			    }
//			    this.inscription =
//				    equilibrageInscription(this.inscription);
//			    this.communFacade.saveOrUpdateInscription(
//				    this.inscription);
//
//			}
//			// PAYE OU NON
//
//			this.listeInscriptions =
//				this.tournoi.getListeInscriptions();
//
//			setModificationGeneral(false);
//			FacesContext.getCurrentInstance().addMessage(null,
//				new FacesMessage(
//					"Sauvegarde effectu�e de l'inscription de "
//						+ this.inscription.getJoueur()
//							.getLabel(),
//					""));
//			actifDouble = true;
//			actifMixte = true;
//
//		    } else if (this.edit == false) {
//			for (String ano : listeAnomalies) {
//			    FacesContext.getCurrentInstance().addMessage(null,
//				    new FacesMessage(
//					    FacesMessage.SEVERITY_ERROR, ano,
//					    " "));
//			}
//			this.inscription.setCategorieSimple(false);
//			this.inscription.setCategorieDouble(false);
//			this.inscription.setCategorieMixte(false);
//			this.inscription.setPartenaireDouble(null);
//			this.inscription.setPartenaireMixte(null);
//			this.selectedJoueurWsDouble = null;
//			this.selectedJoueurWsMixte = null;
//		    }
//
//		    if (paye == null) {
//			try {
//			    this.communFacade.saveOrUpdateInscription(
//				    this.inscription);
//			    setModificationGeneral(false);
//			    FacesContext.getCurrentInstance().addMessage(null,
//				    new FacesMessage(
//					    "Modifications de votre inscription prisent en compte ",
//					    ""));
//			    actifDouble = true;
//			    actifMixte = true;
//			} catch (Exception e) {
//			    e.printStackTrace();
//			}
//		    }
//
//		} else {
//		    FacesContext.getCurrentInstance().addMessage(null,
//			    new FacesMessage(FacesMessage.SEVERITY_ERROR,
//				    "Vous ne pouvez pas faire autant de tableaux",
//				    " "));
//		}
//
//	    } else {
//		FacesContext.getCurrentInstance().addMessage(null,
//			new FacesMessage(FacesMessage.SEVERITY_ERROR,
//				"Vous devez choisir au moins une discipline pour vous inscrire",
//				" "));
//	    }
//
//	} catch (
//
//	final Exception e) {
//
//	    // message d erreur
//	    FacesContext.getCurrentInstance().addMessage(null,
//		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
//			    e.getMessage(), " "));
//	}
//
//    }
//
//    public void editInscription() {
//	setModificationGeneral(true);
//	this.edit = true;
//    }
//
//    public void annuler() {
//
//	setModificationGeneral(false);
//	this.inscription =
//		this.communFacade.findInscription(this.inscription.getId());
//
//    }
//
//    public String delete() {
//
//	try {
//
//	    this.communFacade.deleteInscription(this.inscription);
//
//	    FacesContext.getCurrentInstance().addMessage(null,
//		    new FacesMessage(
//			    "Suppression de l'inscription reussi " + ""));
//
//	    return "/jsf/tournois/fichetournoi.xhtml?true&includeViewParams=true";
//	} catch (Exception e) {
//	    e.printStackTrace();
//	}
//	return null;
//
//    }
//
//    public List<JoueurWsDTO> rechercheExterne(String query) {
//	this.listeJoueursAutocomplete = new ArrayList<>();
//	this.listeJoueursAutocomplete =
//		this.communFacade.rechercheListeExterne(query);
//	return listeJoueursAutocomplete;
//    }
//
//    public List<JoueurWsDTO> rechercheExterneMixte(String query) {
//	this.listeJoueursAutocompleteMixte = new ArrayList<>();
//	this.listeJoueursAutocompleteMixte =
//		this.communFacade.rechercheListeExterne(query);
//	return listeJoueursAutocompleteMixte;
//    }
//
//}

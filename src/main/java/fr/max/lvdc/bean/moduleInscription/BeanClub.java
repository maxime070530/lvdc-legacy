package fr.max.lvdc.bean.moduleInscription;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.exception.ValidationException;
import fr.max.lvdc.facade.api.ICommunFacade;

@Component("beanClub")
@Scope("view")
@Lazy(true)
public class BeanClub extends AbstractGenericBean {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(BeanClub.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 3634308499221999277L;

	// injection
	@Autowired
	private ICommunFacade communFacade;
	// injection

	private ClubPOJO club = null;

	@PostConstruct
	public void init() {
		final String idClub = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap().get("idClub");
		if (idClub != null) {
			this.club = this.communFacade.findClub(Integer.parseInt(idClub));
		} else {
			this.club = new ClubPOJO();
			setModificationGeneral(true);
		}
	}

	public ClubPOJO getClub() {
		return club;
	}

	public void setClub(ClubPOJO club) {
		this.club = club;
	}

	/**
     * 
     */
	public void save() {

		try {

			this.communFacade.saveClub(this.club);

			setModificationGeneral(false);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Sauvegarde effectu�e du club", " "));

		} catch (final Exception e) {

			// message d erreur
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e
							.getMessage(), " "));
		}

	}

	/**
     * 
     */
	public void annuler() {

		setModificationGeneral(false);
		this.club = this.communFacade.findClub(this.club.getId());

	}

	public String delete() {
		try {

			this.communFacade.deleteClub(this.club);

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Suppression du club ", " "));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);
		} catch (ValidationException ve) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, ve
							.getMessage(), " "));

			return null;
		}

		return "LISTE_CLUBS";

	}

	@Override
	public String getLibelleHeader() {
		return super.getLibelleHeader("Club", this.club.getNomClub());
	}
}

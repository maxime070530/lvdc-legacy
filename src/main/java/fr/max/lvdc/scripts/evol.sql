ALTER TABLE inscription ADD paye int4 NOT NULL;
ALTER TABLE inscription ADD prix_inscription int4 NOT NULL;

ALTER TABLE joueur ADD tournoi_paye int4;
ALTER TABLE joueur ADD tournoi_gardois_paye int4;

ALTER TABLE tournoi ADD tour_gardois int4 default 0;
ALTER TABLE tournoi ADD isActif int4 default 0;

INSERT INTO public.role
(id_role, libelle, code)
VALUES (4, 'Comptable', 'COMPTA');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (5, 'Gestionnaire Tournoi', 'TOURN');

ALTER TABLE joueur ADD cote_simple int4;
ALTER TABLE joueur ADD cote_double int4;
ALTER TABLE joueur ADD cote_mixte int4;
ALTER TABLE joueur ADD classement_simple varchar;
ALTER TABLE joueur ADD classement_double varchar;
ALTER TABLE joueur ADD classement_mixte varchar;


ALTER TABLE categorie_tournoi 
DROP COLUMN classement_max,
DROP COLUMN classement_min;

ALTER TABLE joueur 
DROP COLUMN classement_simple CASCADE,
DROP COLUMN classement_double CASCADE,
DROP COLUMN classement_mixte CASCADE;

ALTER TABLE inscription 
DROP COLUMN classement_simple CASCADE,
DROP COLUMN classement_double CASCADE,
DROP COLUMN classement_mixte CASCADE,
DROP COLUMN partenaire_externe_double CASCADE,
DROP COLUMN partenaire_externe_mixte CASCADE;

DROP Table classement CASCADE;

ALTER TABLE joueur DROP COLUMN prenom;

ALTER TABLE tournoi ADD score_center int4 default 0;
ALTER TABLE tournoi ADD lien_score_center varchar;







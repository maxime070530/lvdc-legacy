
package fr.max.lvdc.core;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.web.context.ContextLoader;

import fr.max.lvdc.bean.BeanLockPage;

public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(final HttpSessionEvent event) {

    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent event) {

	BeanLockPage beanLockPage =
		ContextLoader.getCurrentWebApplicationContext().getBean(
			BeanLockPage.class);

	beanLockPage.removeLockSessionDestroy(event.getSession().getId());

    }
}

package fr.max.lvdc.core;

import java.nio.charset.Charset;
import java.security.MessageDigest;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.encoding.BasePasswordEncoder;
import org.springframework.stereotype.Component;

@Component("passwordEncoder")
public class PasswordHelper extends BasePasswordEncoder {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(PasswordHelper.class);

	private MessageDigest md = null;
	// static private MD5 md5 = null;
	private static final char[] HEXCHARS = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	/**
	 * Instantiates a new password helper.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public PasswordHelper() throws Exception {
		md = MessageDigest.getInstance("MD5");
	}

	/**
	 * Decrypt.
	 * 
	 * @param value
	 *            the value
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public String encrypt(final String value) throws Exception {

		return this.hashData(value.getBytes(Charset.defaultCharset()));
	}

	/**
	 * @param dataToHash
	 *            byte[]
	 * @return String hashData
	 */
	public String hashData(final byte[] dataToHash) {
		return hexStringFromBytes(calculateHash(dataToHash));
	}

	/**
	 * @param dataToHash
	 *            byte[]
	 * @return byte[] calculateHash
	 */
	private byte[] calculateHash(final byte[] dataToHash) {
		md.update(dataToHash, 0, dataToHash.length);
		return md.digest();
	}

	private static final int HEXA = 16;
	private static final int CLE = 0x000000FF;

	/**
	 * @param b
	 *            byte[]
	 * @return String hexStringFromBytes
	 */
	public String hexStringFromBytes(final byte[] b) {
		final StringBuilder hex = new StringBuilder("");
		int msb;
		int lsb = 0;
		int i;
		// MSB maps to idx 0
		for (i = 0; i < b.length; i++) {
			msb = (b[i] & CLE) / HEXA;
			lsb = (b[i] & CLE) % HEXA;
			hex.append(HEXCHARS[msb]).append(HEXCHARS[lsb]);
		}
		return hex.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.authentication.encoding.PasswordEncoder#
	 * encodePassword(java.lang.String, java.lang.Object)
	 */
	@Override
	public String encodePassword(final String rawPass, final Object salt)
			throws DataAccessException {
		try {
			return mergePasswordAndSalt(encrypt(rawPass), salt, true);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.authentication.encoding.PasswordEncoder#
	 * isPasswordValid(java.lang.String, java.lang.String, java.lang.Object)
	 */
	@Override
	public boolean isPasswordValid(final String encPass, final String rawPass,
			final Object salt) throws DataAccessException {
		try {
			return encPass.equals(encrypt(rawPass));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}
}

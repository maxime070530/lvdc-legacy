
package fr.max.lvdc.core;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class RestoreViewExceptionHandlerFactory extends
	ExceptionHandlerFactory {

    private final ExceptionHandlerFactory parent;

    // this injection handles jsf
    public RestoreViewExceptionHandlerFactory(
	    final ExceptionHandlerFactory parent) {
	this.parent = parent;
    }

    @Override
    public ExceptionHandler getExceptionHandler() {

	ExceptionHandler handler =
		new RestoreViewExceptionHandler(parent.getExceptionHandler());

	return handler;
    }

}

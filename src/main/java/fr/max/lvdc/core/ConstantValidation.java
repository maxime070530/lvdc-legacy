
package fr.max.lvdc.core;

/**
 * The Class ConstantMime.
 */
public final class ConstantValidation {

    public static final int MAXSIZE0 = 0;
    public static final int MAXSIZE1 = 1;
    public static final int MAXSIZE2 = 2;
    public static final int MAXSIZE3 = 3;
    public static final int MAXSIZE4 = 4;
    public static final int MAXSIZE5 = 5;
    public static final int MAXSIZE6 = 6;
    public static final int MAXSIZE7 = 7;
    public static final int MAXSIZE8 = 8;
    public static final int MAXSIZE9 = 9;
    public static final int MAXSIZE10 = 10;
    public static final int MAXSIZE11 = 11;
    public static final int MAXSIZE12 = 12;
    public static final int MAXSIZE14 = 14;
    public static final int MAXSIZE15 = 15;

    public static final int MAXSIZE17 = 17;

    public static final int MAXSIZE20 = 20;

    public static final int MAXSIZE30 = 30;

    public static final int MAXSIZE32 = 32;

    public static final int MAXSIZE38 = 38;

    public static final int MAXSIZE40 = 40;

    public static final int MAXSIZE50 = 50;

    public static final int MAXSIZE60 = 60;

    public static final int MAXSIZE75 = 75;

    public static final int MAXSIZE100 = 100;
    public static final int MAXSIZE110 = 110;
    public static final int MAXSIZE120 = 120;

    public static final int MAXSIZE140 = 140;

    public static final int MAXSIZE150 = 150;

    public static final int MAXSIZE200 = 200;

    public static final int MAXSIZE240 = 240;

    public static final int MAXSIZE255 = 255;
    public static final int MAXSIZE256 = 256;

    public static final int MAXSIZE500 = 500;

    public static final int MAXSIZE1000 = 1000;

    public static final int MAXSIZE3000 = 3000;

    public static final int MAXSIZE4000 = 4000;

    public static final int MAXSIZE1300 = 7300;

    public static final int MAXSIZE10000 = 10000;

}

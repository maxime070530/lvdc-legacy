
package fr.max.lvdc.core;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NamedParamStatement {

    private final PreparedStatement prepStmt;
    private final List<String> fields = new ArrayList<String>();

    public NamedParamStatement(final Connection conn, String sql)
	    throws SQLException {
	int pos;
	while ((pos = sql.indexOf("#{")) != -1) {
	    int end = sql.substring(pos).indexOf("}");
	    if (end == -1) {
		end = sql.length();
	    } else {
		end += pos;
	    }
	    fields.add(sql.substring(pos + 2, end));
	    sql = sql.substring(0, pos) + "?" + sql.substring(end + 1);
	}
	prepStmt = conn.prepareStatement(sql);
    }

    public PreparedStatement getPreparedStatement() {
	return prepStmt;
    }

    public ResultSet executeQuery() throws SQLException {
	return prepStmt.executeQuery();
    }

    public void close() throws SQLException {
	prepStmt.close();
    }

    public void setObject(final int paramIndex, final Object value)
	    throws SQLException {
	prepStmt.setObject(paramIndex, value);
    }

    // public void setObject(final String name, final Object value)
    // throws SQLException {
    // prepStmt.setObject(getIndex(name), value);
    // }
    //
    // private int getIndex(final String name) {
    // return fields.indexOf(name) + 1;
    // }

    /**
     * @return the fields
     */
    public List<String> getFields() {
	return fields;
    }
}

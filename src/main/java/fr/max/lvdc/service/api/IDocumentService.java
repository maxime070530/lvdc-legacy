package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.DocumentPOJO;

public interface IDocumentService {

	void saveDocument(DocumentPOJO document);

	List<DocumentPOJO> findDocumentByTournoi(Integer idTournoi);

	void deleteDocument(DocumentPOJO document);

	DocumentPOJO findDocument(int idDocument);

}

package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;

public interface IClubService {

	List<ClubPOJO> findListClubByFiltre(FiltreClubDTO filtre);

	List<ClubPOJO> findAllClub();

	void saveClub(ClubPOJO club);

	ClubPOJO findClub(int idClub);

	void updateClub(ClubPOJO club);

	void deleteClub(ClubPOJO club);

	ClubPOJO findOneClubByFiltre(FiltreClubDTO filtre);
	
}

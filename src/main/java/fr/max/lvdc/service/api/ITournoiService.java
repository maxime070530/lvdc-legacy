package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;

public interface ITournoiService {

	List<TournoiPOJO> findAllTournoi();

	TournoiPOJO findTournoi(int idTournoi);

	void deleteTournoi(TournoiPOJO tournoi);

	void saveTournoi(TournoiPOJO tournoi);

	List<TournoiPOJO> findAllTournoiByDate();

	List<TournoiPOJO> findAllTournoiOrderByDate();

}

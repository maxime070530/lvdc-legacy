
package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreInscriptionDTO;

public interface IInscriptionService {

    List<InscriptionPOJO> findAllInscriptions();

    InscriptionPOJO findInscription(int idInscription);

    void deleteInscription(InscriptionPOJO inscription);

    List<InscriptionPOJO> findInscriptionByTournoi(int idTournoi);

    void updateInscription(InscriptionPOJO inscription);

    List<InscriptionPOJO>
	    findAllInscriptionsByFiltre(FiltreInscriptionDTO filtre);

    void saveOrUpdateInscription(InscriptionPOJO inscription);

}

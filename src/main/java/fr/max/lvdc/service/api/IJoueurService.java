
package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;

public interface IJoueurService {

    List<JoueurPOJO> findAllJoueur();

    JoueurPOJO findJoueur(int idJoueur);

    void deleteJoueur(JoueurPOJO joueur);

    void saveJoueur(JoueurPOJO joueur);

    void updateJoueur(JoueurPOJO joueur);

    List<JoueurPOJO> findJoueurByFiltre(FiltreJoueurDTO filtre);

    JoueurPOJO findJoueurEager(Integer idJoueur);

    List<JoueurPOJO> findListeJoueursWhereNoCompte();

    JoueurPOJO getJoueurByLicence(String licence);

}

package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.CategorieTournoiPOJO;

public interface ICategorieTournoiService {

	List<CategorieTournoiPOJO> findAllCategTournoi();

	CategorieTournoiPOJO findCategorieTournoi(int idCategorie);

}

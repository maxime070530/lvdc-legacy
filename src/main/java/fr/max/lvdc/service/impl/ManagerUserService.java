
package fr.max.lvdc.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.max.lvdc.dao.api.IRoleDAO;
import fr.max.lvdc.dao.api.IUserDAO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.service.api.IManagerUserService;

@Service("managerUserService")
public class ManagerUserService implements IManagerUserService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER =
	    Logger.getLogger(ManagerUserService.class);

    // DEBUT INJECTION SPRING.

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IRoleDAO roleDAO;

    // FIN INJECTION SPRING.

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public UserPOJO getUserByLogin(final String username) {

	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("recherche user pour connection");
	}

	// Recherche du USER par son LOGIN
	return this.userDAO.findByLogin(username);
    }

    @Override
    public void saveMonCompte(final UserPOJO utilisateur) {
	this.userDAO.saveOrUpdate(utilisateur);
    }

    @Override
    public UserPOJO findUserById(Integer idUtilisateur) {
	return this.userDAO.get(idUtilisateur);
    }

    @Override
    public List<UserPOJO> findAllUser() {
	return this.userDAO.findAll();
    }

    @Override
    public List<RolePOJO> findAllRole() {
	return this.roleDAO.findAll();
    }

    @Override
    public RolePOJO getRole(int idRole) {
	return this.roleDAO.get(idRole);
    }

    @Override
    public UserPOJO findUserByJoueur(Integer idJoueur) {
	return this.userDAO.findUserByJoueur(idJoueur);
    }

    @Override
    public void deleteUser(UserPOJO user) {
	this.userDAO.delete(user);
    }

}

package fr.max.lvdc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.max.lvdc.dao.api.IVeteranDAO;
import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;
import fr.max.lvdc.service.api.IVeteranService;

@Service("veteranService")
public class VeteranService implements IVeteranService {
	
	@Autowired
	private IVeteranDAO veteranDAO;
	
	@Override
	public List<VeteranPOJO> findAll(){
		return this.veteranDAO.findAll();
	}
	
	@Override
	public VeteranPOJO findVeteran(int idVeteran){
		return this.veteranDAO.get(idVeteran);
	}
}

package fr.max.lvdc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.max.lvdc.dao.api.IClubDAO;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.service.api.IClubService;

@Service("clubService")
public class ClubService implements IClubService {

	@Autowired
	private IClubDAO clubDAO;

	@Override
	public List<ClubPOJO> findListClubByFiltre(FiltreClubDTO filtre) {
		return this.clubDAO.findClubByFiltre(filtre);
	}

	@Override
	public List<ClubPOJO> findAllClub() {
		return this.clubDAO.findAll();
	}

	@Override
	public void saveClub(ClubPOJO club) {
		this.clubDAO.saveOrUpdate(club);
	}

	@Override
	public ClubPOJO findClub(int idClub) {
		return this.clubDAO.get(idClub);
	}

	@Override
	public void updateClub(ClubPOJO club) {
		this.clubDAO.saveOrUpdate(club);
	}

	@Override
	public void deleteClub(ClubPOJO club) {
		this.clubDAO.delete(club);
	}

	@Override
	public ClubPOJO findOneClubByFiltre(FiltreClubDTO filtre) {
		return this.clubDAO.findOneClubByFiltre(filtre);
	}

}

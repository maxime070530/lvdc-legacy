package fr.max.lvdc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.max.lvdc.dao.api.IDocumentDAO;
import fr.max.lvdc.domaine.moduleinscription.DocumentPOJO;
import fr.max.lvdc.service.api.IDocumentService;

@Service("documentService")
public class DocumentService implements IDocumentService {

	@Autowired
	private IDocumentDAO documentDAO;

	@Override
	public void saveDocument(DocumentPOJO document) {
		this.documentDAO.saveOrUpdate(document);
	}

	@Override
	public List<DocumentPOJO> findDocumentByTournoi(Integer idTournoi) {
		return this.documentDAO.findDocumentByTournoi(idTournoi);
	}
	
	@Override
	public void deleteDocument(DocumentPOJO document){
		this.documentDAO.delete(document);
	}
	
	@Override
	public DocumentPOJO findDocument(int idDocument){
		return this.documentDAO.get(idDocument);
	}

}

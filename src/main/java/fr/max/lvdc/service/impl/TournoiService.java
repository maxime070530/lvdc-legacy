package fr.max.lvdc.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.max.lvdc.dao.api.ITournoiDAO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.service.api.ITournoiService;

@Service("tournoiService")
public class TournoiService implements ITournoiService {
	// DEBUT INJECTION
	@Autowired
	private ITournoiDAO tournoiDAO;

	// FIN INJECTION

	@Override
	public List<TournoiPOJO> findAllTournoi() {
		return this.tournoiDAO.findAll();
	}

	@Override
	public TournoiPOJO findTournoi(int idTournoi) {
		return this.tournoiDAO.get(idTournoi);
	}

	@Override
	public void deleteTournoi(TournoiPOJO tournoi) {
		this.tournoiDAO.delete(tournoi);
	}

	@Override
	public void saveTournoi(TournoiPOJO tournoi) {
		this.tournoiDAO.saveOrUpdate(tournoi);
	}

	@Override
	public List<TournoiPOJO> findAllTournoiByDate() {
		final List<String[]> ordres = new ArrayList<String[]>();
		ordres.add(new String[] { "desc", "dateTournoi" });
		return this.tournoiDAO.findTournoiByDate();
	}

	@Override
	public List<TournoiPOJO> findAllTournoiOrderByDate() {
		final List<String[]> ordres = new ArrayList<String[]>();
		ordres.add(new String[] { "asc", "dateTournoi" });
		return this.tournoiDAO.findAll(ordres);
	}

}

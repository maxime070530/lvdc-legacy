package fr.max.lvdc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.max.lvdc.dao.api.ICategorieTournoiDAO;
import fr.max.lvdc.domaine.moduleinscription.CategorieTournoiPOJO;
import fr.max.lvdc.service.api.ICategorieTournoiService;

@Service("categorieTournoiService")
public class CategorieTournoiService implements ICategorieTournoiService {
	// debut injection
	@Autowired
	private ICategorieTournoiDAO categorieTournoiDAO;

	// fin injection

	@Override
	public List<CategorieTournoiPOJO> findAllCategTournoi() {
		return this.categorieTournoiDAO.findAll();
	}

	@Override
	public CategorieTournoiPOJO findCategorieTournoi(int idCategorie) {
		return this.categorieTournoiDAO.get(idCategorie);
	}

}

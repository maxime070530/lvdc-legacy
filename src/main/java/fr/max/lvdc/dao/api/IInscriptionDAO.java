
package fr.max.lvdc.dao.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreInscriptionDTO;

public interface IInscriptionDAO
	extends IGenericDAO<InscriptionPOJO, Integer> {

    List<InscriptionPOJO> findlisteInscriptionsByTournoi(Integer idTournoi);

    List<InscriptionPOJO>
	    findAllInscriptionByFiltre(FiltreInscriptionDTO filtre);

}

package fr.max.lvdc.dao.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.DocumentPOJO;

public interface IDocumentDAO extends IGenericDAO<DocumentPOJO, Integer>{

	List<DocumentPOJO> findDocumentByTournoi(Integer idTournoi);

	

}

package fr.max.lvdc.dao.api;

import fr.max.lvdc.domaine.moduleinscription.RolePOJO;

public interface IRoleDAO extends IGenericDAO<RolePOJO, Integer>{

}

package fr.max.lvdc.dao.api;

import fr.max.lvdc.domaine.moduleinscription.CategorieTournoiPOJO;

public interface ICategorieTournoiDAO extends IGenericDAO<CategorieTournoiPOJO, Integer> {

}

package fr.max.lvdc.dao.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;

public interface ITournoiDAO extends IGenericDAO<TournoiPOJO, Integer> {

    List<TournoiPOJO> findTournoiByDate();

}

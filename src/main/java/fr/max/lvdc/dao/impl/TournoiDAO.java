
package fr.max.lvdc.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.ITournoiDAO;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;

@Repository("tournoiDAO")
public class TournoiDAO extends GenericHibernateDaoImpl<TournoiPOJO, Integer>
	implements ITournoiDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<TournoiPOJO> findTournoiByDate() {

	final Map<String, Object> mapParameters =
		new HashMap<String, Object>();

	final StringBuilder hqlQueryWhere = new StringBuilder();

	final StringBuilder hqlQuery = new StringBuilder(
		" select distinct(tournoi) from TournoiPOJO as tournoi ");
	hqlQuery.append(
		" left join fetch tournoi.listeInscriptions as inscription ");
	hqlQuery.append(" where 1 = 1 ");
	hqlQuery.append(" and tournoi.dateTournoi >= :currentDate ");
	mapParameters.put("currentDate", new Date());

	final Query query =
		em.createQuery(hqlQuery.append(hqlQueryWhere).toString());
	if (mapParameters != null && !mapParameters.isEmpty()) {

	    for (final Map.Entry<String, Object> mapEntry : mapParameters
		    .entrySet()) {
		query.setParameter(mapEntry.getKey(), mapEntry.getValue());
	    }
	}

	return query.getResultList();
    }

}


package fr.max.lvdc.dao.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.IJoueurDAO;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;

@Repository("joueurDAO")
public class JoueurDAO extends GenericHibernateDaoImpl<JoueurPOJO, Integer>
	implements IJoueurDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<JoueurPOJO> findJoueurByFiltre(final FiltreJoueurDTO filtre) {

	final Map<String, Object> mapParameters =
		new HashMap<String, Object>();

	final StringBuilder hqlQueryWhere = new StringBuilder();
	StringBuilder hqlQuery = new StringBuilder();

	if (filtre.getConpta() == true) {

	    hqlQuery.append(
		    " select distinct(joueur) from JoueurPOJO as joueur ");
	    hqlQuery.append(
		    " left join fetch joueur.listeInscriptions as inscription ");

	    hqlQuery.append(" where 1 = 1 ");

	} else {
	    hqlQuery.append("  from JoueurPOJO as joueur ");
	    hqlQuery.append(" where 1 = 1 ");
	}

	if (filtre.getNomJoueur() != null
		&& !filtre.getNomJoueur().trim().isEmpty()) {
	    hqlQuery.append(
		    "  and upper(joueur.nomJoueur) like upper(:nomJoueur) ");
	    mapParameters.put("nomJoueur", "%" + filtre.getNomJoueur() + "%");
	}

	if (filtre.getLicenceJoueur() != null
		&& !filtre.getLicenceJoueur().trim().isEmpty()) {
	    hqlQuery.append(
		    "  and upper(joueur.licenceJoueur) like upper(:licenceJoueur) ");
	    mapParameters.put("licenceJoueur",
		    "%" + filtre.getLicenceJoueur() + "%");
	}

	if (filtre.getSexe() != null && !filtre.getSexe().trim().isEmpty()) {
	    hqlQuery.append(
		    "  and upper(joueur.sexeJoueur) like upper(:sexeJoueur) ");
	    mapParameters.put("sexeJoueur", "%" + filtre.getSexe() + "%");
	}

	if (filtre.getInterne() != null) {

	    if (filtre.getInterne()) {
		hqlQuery.append("  and joueur.externe = false ");
	    } else {
		hqlQuery.append("  and joueur.externe = true");
	    }

	}

	// if (filtre.getInscriptionNull() != null) {
	//
	// if (filtre.getInscriptionNull()) {
	// hqlQuery.append(" and joueur.listeInscriptions is empty ");
	// } else {
	// hqlQuery.append(" and joueur.listeInscriptions is not empty ");
	// }
	//
	// }

	// FAIRE UN FILTRE LISTE NULL

	if (filtre.getIntervalle() != null && filtre.getConpta() == true) {

	    Date date = new Date();
	    Calendar cal = Calendar.getInstance();

	    if (filtre.getIntervalle() == 1) {
		hqlQuery.append(
			" and inscription.dateInscription <= :currentDate ");
	    } else if (filtre.getIntervalle() == 2) {
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1);
		date = cal.getTime();
		hqlQuery.append(
			" and inscription.dateInscription >= :oneMonth ");
		hqlQuery.append(
			" and inscription.dateInscription <= :currentDate ");
		mapParameters.put("oneMonth", date);
	    } else if (filtre.getIntervalle() == 3) {
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -3);
		date = cal.getTime();
		hqlQuery.append(
			" and inscription.dateInscription >= :threeMonth ");
		hqlQuery.append(
			" and inscription.dateInscription <= :currentDate ");
		mapParameters.put("threeMonth", date);
	    } else if (filtre.getIntervalle() == 4) {
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -6);
		date = cal.getTime();
		hqlQuery.append(
			" and inscription.dateInscription >= :sixMonth ");
		hqlQuery.append(
			" and inscription.dateInscription <= :currentDate ");
		mapParameters.put("sixMonth", date);
	    } else if (filtre.getIntervalle() == 5) {
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -12);
		date = cal.getTime();
		hqlQuery.append(
			" and inscription.dateInscription >= :twelveMonth ");
		hqlQuery.append(
			" and inscription.dateInscription <= :currentDate ");
		mapParameters.put("twelveMonth", date);
	    }

	    mapParameters.put("currentDate", new Date());

	}

	final Query query =
		em.createQuery(hqlQuery.append(hqlQueryWhere).toString());

	if (mapParameters != null && !mapParameters.isEmpty()) {

	    for (final Map.Entry<String, Object> mapEntry : mapParameters
		    .entrySet()) {
		query.setParameter(mapEntry.getKey(), mapEntry.getValue());
	    }
	}

	return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<JoueurPOJO> findlisteJoueursWhereNoCompte() {
	final Map<String, Object> mapParameters =
		new HashMap<String, Object>();

	final StringBuilder hqlQuery =
		new StringBuilder(" from JoueurPOJO as joueur ");
	hqlQuery.append(" left join fetch joueur.user as user ");
	hqlQuery.append(" where 1 = 1 ");

	hqlQuery.append(" and user = null ");

	final Query query = em.createQuery(hqlQuery.toString());

	if (mapParameters != null && !mapParameters.isEmpty()) {

	    for (final Map.Entry<String, Object> mapEntry : mapParameters
		    .entrySet()) {
		query.setParameter(mapEntry.getKey(), mapEntry.getValue());
	    }
	}
	return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public JoueurPOJO findJoueurByLicence(final String licence) {

	final Map<String, Object> mapParameters =
		new HashMap<String, Object>();

	final StringBuilder hqlQueryWhere = new StringBuilder();

	final StringBuilder hqlQuery =
		new StringBuilder(" from JoueurPOJO as joueur ");
	hqlQuery.append(" where 1 = 1 ");

	hqlQuery.append(
		"  and upper(joueur.licenceJoueur) like upper(:licenceJoueur) ");
	mapParameters.put("licenceJoueur", "%" + licence + "%");

	final Query query =
		em.createQuery(hqlQuery.append(hqlQueryWhere).toString());

	if (mapParameters != null && !mapParameters.isEmpty()) {

	    for (final Map.Entry<String, Object> mapEntry : mapParameters
		    .entrySet()) {
		query.setParameter(mapEntry.getKey(), mapEntry.getValue());
	    }
	}

	return (JoueurPOJO) query.getSingleResult();
    }

}

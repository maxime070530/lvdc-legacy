package fr.max.lvdc.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.IClubDAO;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;

@Repository("clubDAO")
public class ClubDAO extends GenericHibernateDaoImpl<ClubPOJO, Integer> implements IClubDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ClubPOJO> findClubByFiltre(final FiltreClubDTO filtre) {

		final Map<String, Object> mapParameters = new HashMap<String, Object>();

		final StringBuilder hqlQueryWhere = new StringBuilder();

		final StringBuilder hqlQuery = new StringBuilder(" from ClubPOJO as club ");
		hqlQuery.append(" where 1 = 1 ");

		if (filtre.getNomClub() != null && !filtre.getNomClub().trim().isEmpty()) {
			hqlQuery.append("  and upper(club.nomClub) like upper(:nomClub) ");
			mapParameters.put("nomClub", "%" + filtre.getNomClub() + "%");
		}

		if (filtre.getVilleClub() != null && !filtre.getVilleClub().trim().isEmpty()) {
			hqlQuery.append("  and upper(club.villeClub) like upper(:villeClub) ");
			mapParameters.put("villeClub", "%" + filtre.getVilleClub() + "%");
		}

		if (filtre.getAbrevClub() != null && !filtre.getAbrevClub().trim().isEmpty()) {
			hqlQuery.append("  and upper(club.abrevClub) like upper(:abrevClub) ");
			mapParameters.put("abrevClub", "%" + filtre.getAbrevClub() + "%");
		}

		final Query query = em.createQuery(hqlQuery.append(hqlQueryWhere).toString());

		if (mapParameters != null && !mapParameters.isEmpty()) {

			for (final Map.Entry<String, Object> mapEntry : mapParameters.entrySet()) {
				query.setParameter(mapEntry.getKey(), mapEntry.getValue());
			}
		}

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ClubPOJO findOneClubByFiltre(final FiltreClubDTO filtre) {

		final Map<String, Object> mapParameters = new HashMap<String, Object>();

		final StringBuilder hqlQueryWhere = new StringBuilder();

		final StringBuilder hqlQuery = new StringBuilder(" from ClubPOJO as club ");
		hqlQuery.append(" where 1 = 1 ");

		if (filtre.getNomClub() != null && !filtre.getNomClub().trim().isEmpty()) {
			hqlQuery.append("  and upper(club.nomClub) like upper(:nomClub) ");
			mapParameters.put("nomClub", "%" + filtre.getNomClub() + "%");
		}

		if (filtre.getVilleClub() != null && !filtre.getVilleClub().trim().isEmpty()) {
			hqlQuery.append("  and upper(club.villeClub) like upper(:villeClub) ");
			mapParameters.put("villeClub", "%" + filtre.getVilleClub() + "%");
		}

		if (filtre.getAbrevClub() != null && !filtre.getAbrevClub().trim().isEmpty()) {
			hqlQuery.append("  and upper(club.abrevClub) like upper(:abrevClub) ");
			mapParameters.put("abrevClub", "%" + filtre.getAbrevClub() + "%");
		}

		final Query query = em.createQuery(hqlQuery.append(hqlQueryWhere).toString());

		if (mapParameters != null && !mapParameters.isEmpty()) {

			for (final Map.Entry<String, Object> mapEntry : mapParameters.entrySet()) {
				query.setParameter(mapEntry.getKey(), mapEntry.getValue());
			}
		}

		return (ClubPOJO) query.getSingleResult();
	}

}

package fr.max.lvdc.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.IUserDAO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;

@Repository("userDAO")
public class UserDAO extends GenericHibernateDaoImpl<UserPOJO, Integer>
		implements IUserDAO {

	@Override
	public UserPOJO findByLogin(final String username) {

		final StringBuilder hqlQuery = new StringBuilder(
				" from UserPOJO as user ");
		hqlQuery.append(" where UPPER(user.nom) = UPPER(:login) ");
		final Query query = em.createQuery(hqlQuery.toString());
		query.setParameter("login", username);
		return (UserPOJO) query.getSingleResult();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserPOJO> findAllUsers() {
		final StringBuilder hqlQuery = new StringBuilder(
				" from UserPOJO as user ");
		hqlQuery.append(" order by user.nom ");
		final Query query = em.createQuery(hqlQuery.toString());

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserPOJO findUserByJoueur(Integer idJoueur) {
		final Map<String, Object> mapParameters = new HashMap<String, Object>();

		final StringBuilder hqlQuery = new StringBuilder(
				" from UserPOJO as user ");
		hqlQuery.append(" left join fetch user.joueur as joueur ");
		hqlQuery.append(" where 1 = 1 ");

		mapParameters.put("idJoueur", idJoueur);
		hqlQuery.append(" and joueur.id = :idJoueur  ");

		final Query query = em.createQuery(hqlQuery.toString());

		if (mapParameters != null && !mapParameters.isEmpty()) {

			for (final Map.Entry<String, Object> mapEntry : mapParameters
					.entrySet()) {
				query.setParameter(mapEntry.getKey(), mapEntry.getValue());
			}
		}
		try {
			return (UserPOJO) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}

	}

}

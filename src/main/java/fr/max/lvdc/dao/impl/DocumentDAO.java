package fr.max.lvdc.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.IDocumentDAO;
import fr.max.lvdc.domaine.moduleinscription.DocumentPOJO;

@Repository("documentDAO")
public class DocumentDAO extends GenericHibernateDaoImpl<DocumentPOJO, Integer>
		implements IDocumentDAO {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentPOJO> findDocumentByTournoi(Integer idTournoi) {

		final Map<String, Object> mapParameters = new HashMap<String, Object>();

		final StringBuilder hqlQuery = new StringBuilder(
				" from DocumentPOJO as document ");
		hqlQuery.append(" left join fetch document.tournoi as tournoi ");
		hqlQuery.append(" where 1 = 1 ");

		if (idTournoi != null) {
			mapParameters.put("idTournoi", idTournoi);
			hqlQuery.append(" and tournoi.id = :idTournoi ");
		}

		final Query query = em.createQuery(hqlQuery.toString());

		if (mapParameters != null && !mapParameters.isEmpty()) {

			for (final Map.Entry<String, Object> mapEntry : mapParameters
					.entrySet()) {
				query.setParameter(mapEntry.getKey(), mapEntry.getValue());
			}
		}
		return query.getResultList();
	}

}

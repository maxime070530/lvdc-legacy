package fr.max.lvdc.dao.impl;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.IVeteranDAO;
import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;

@Repository("veteranDAO")
public class VeteranDAO extends GenericHibernateDaoImpl<VeteranPOJO, Integer>
		implements IVeteranDAO {

}

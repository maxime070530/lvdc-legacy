
package fr.max.lvdc.facade.api;

import java.io.File;
import java.io.IOException;
import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;

public interface IExportFacade {

    File exportInscriptions(List<InscriptionPOJO> listeInscriptions, TournoiPOJO tournoi)
	    throws IOException;

    File exportCompta(List<JoueurPOJO> listeJoueurs) throws IOException;

}

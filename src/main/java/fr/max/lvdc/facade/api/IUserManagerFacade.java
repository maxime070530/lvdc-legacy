
package fr.max.lvdc.facade.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;

/**
 * @author kcamroux
 */
public interface IUserManagerFacade {

    /**
     * @param utilisateur
     */
    void saveMonCompte(UserPOJO utilisateur);

    List<UserPOJO> findAllUsers();

    UserPOJO findUserByCode(String username, boolean initialize);

    List<RolePOJO> findAllRole();

    RolePOJO getRole(int idRole);

    UserPOJO findUserByJoueur(Integer idJoueur);

    UserPOJO findUserById(Integer idUtilisateur);

    void deleteUser(UserPOJO user);
}


package fr.max.lvdc.facade.api;

import java.io.File;
import java.io.IOException;
import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.CategorieTournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.DocumentPOJO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.dto.muduleinscription.FiltreInscriptionDTO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.dto.ws.JoueurWsDTO;

public interface ICommunFacade {

    List<TournoiPOJO> findAllTournoi();

    TournoiPOJO findTournoi(int idTournoi);

    List<CategorieTournoiPOJO> findAllCategorieTournoi();

    CategorieTournoiPOJO findCategorieTournoi(int idCategorie);

    void updateJoueur(JoueurPOJO joueur);

    void saveJoueur(JoueurPOJO joueur);

    void deleteJoueur(JoueurPOJO joueur);

    JoueurPOJO findJoueur(int idJoueur);

    List<JoueurPOJO> findAllJoueur();

    void deleteTournoi(TournoiPOJO tournoi);

    void saveTournoi(TournoiPOJO tournoi);

    List<InscriptionPOJO> findAllInscriptions();

    InscriptionPOJO findInscription(int idInscription);

    void deleteInscription(InscriptionPOJO inscription);

    void saveOrUpdateInscription(InscriptionPOJO inscription);

    List<InscriptionPOJO> findInscriptionByTournoi(int idTournoi);

    ClubPOJO findClub(int idClub);

    void updateClub(ClubPOJO club);

    void deleteClub(ClubPOJO club);

    List<JoueurPOJO> findJoueurByFiltre(FiltreJoueurDTO filtre);

    JoueurPOJO findJoueurEager(Integer idJoueur);

    List<ClubPOJO> findClubByFiltre(FiltreClubDTO filtre);

    List<ClubPOJO> findAllClub();

    void saveClub(ClubPOJO club);

    List<TournoiPOJO> findAllTournoiByDate();

    List<TournoiPOJO> findAllTournoiOrderByDate();

    VeteranPOJO findVeteran(int idVeteran);

    void saveDocument(DocumentPOJO document);

    List<DocumentPOJO> findDocumentByTournoi(Integer idTournoi);

    void deleteDocument(DocumentPOJO document);

    void updateInscription(InscriptionPOJO inscription);

    List<JoueurPOJO> findListeJoueursWhereNoCompte();

    ClubPOJO findOneClubByFiltre(FiltreClubDTO filtre);

    List<JoueurWsDTO> rechercheListeExterne(String query);

    void refreshClassement(List<JoueurPOJO> listeJoueurs);

    JoueurPOJO saveJoueurWs(JoueurWsDTO joueurWs);

    List<InscriptionPOJO>
	    findAllInscriptionsByFiltre(FiltreInscriptionDTO filtre);

    JoueurPOJO getJoueurByLicence(String licence);

}

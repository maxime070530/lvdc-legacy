
package fr.max.lvdc.facade.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.dto.muduleinscription.ImportDataDTO;
import fr.max.lvdc.dto.muduleinscription.StatusImportDTO;
import fr.max.lvdc.dto.ws.JoueurWsDTO;
import fr.max.lvdc.facade.api.IImportFacade;
import fr.max.lvdc.service.api.IClubService;
import fr.max.lvdc.service.api.IJoueurService;
import fr.max.lvdc.service.api.IManagerUserService;

@Service("importFacade")
public class ImportFacade implements IImportFacade {

    @Autowired
    private IClubService clubService;

    @Autowired
    private IJoueurService joueurService;

    @Autowired
    private IManagerUserService managerUserService;

    @Override
    public boolean importUser(InputStream file,

	    List<List<String>> listeAnomalies, List<UserPOJO> listeUserToSave,
	    StatusImportDTO status) {

	XSSFWorkbook fileExcel = null;

	try {

	    fileExcel = new XSSFWorkbook(file);

	    final XSSFSheet sheet = fileExcel.getSheetAt(0);
	    final Iterator<Row> rows = sheet.iterator();

	    List<ImportDataDTO> mappingByJoueur = new ArrayList<>();

	    while (rows.hasNext()) {
		final Row row = rows.next();
		if (row.getRowNum() > 0) {
		    ImportDataDTO data = new ImportDataDTO();

		    row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
		    data.setLicence(row.getCell(0).getStringCellValue());

		    mappingByJoueur.add(data);
		    status.setTotalLigneTraiter(
			    status.getTotalLigneTraiter() + 1);
		}
	    }

	    for (ImportDataDTO d : mappingByJoueur) {

		List<JoueurWsDTO> listeJoueursWS =
			new ArrayList<JoueurWsDTO>();

		CloseableHttpClient httpclient = HttpClients.createDefault();

		URIBuilder url = null;
		try {
		    url = new URIBuilder(
			    "https://www.obc-nimes.com/api/joueur");
		} catch (URISyntaxException e2) {
		    // TODO Auto-generated catch block
		    e2.printStackTrace();
		}
		url.addParameter("query", d.getLicence());

		HttpGet getRequest = null;
		try {
		    getRequest = new HttpGet(url.build());
		} catch (URISyntaxException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
//
//		final HttpHost proxy =
//			new HttpHost("proxy.brl.intra", 3128, "http");
//		final RequestConfig config =
//			RequestConfig.custom().setProxy(proxy).build();
//
//		getRequest.setConfig(config);
		getRequest.setHeader("Accept", "application/json");
		getRequest.setHeader("Content-type", "application/json");

		HttpResponse response = null;
		HttpEntity entity = null;

		try {
		    response = httpclient.execute(getRequest);
		    entity = response.getEntity();
		    ObjectMapper mapper = new ObjectMapper();

		    JsonNode root = mapper.readTree(entity.getContent());
		    httpclient.close();

		    List<JoueurWsDTO> joueurs = mapper.readValue(
			    root.traverse(),
			    mapper.getTypeFactory().constructCollectionType(
				    List.class, JoueurWsDTO.class));

		    for (JoueurWsDTO j : joueurs) {
			listeJoueursWS.add(j);
		    }

		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}

		JoueurPOJO joueur = new JoueurPOJO();
		for (JoueurWsDTO j : listeJoueursWS) {
		    joueur.setNomJoueur(j.getNom());
		    joueur.setLicenceJoueur(j.getLicence());
		    joueur.setSexeJoueur(j.getSexe());
		    ClubPOJO club = new ClubPOJO();
		    FiltreClubDTO filtre = new FiltreClubDTO();
		    filtre.setAbrevClub(j.getClub_court());
		    try {
			club = this.clubService.findOneClubByFiltre(filtre);
		    } catch (Exception e) {
			club = new ClubPOJO();
			club.setAbrevClub(j.getClub_court());
			club.setNomClub(j.getClub());
			this.clubService.saveClub(club);
		    }

		    joueur.setClub(club);
		    joueur.setClassementSimple(j.getClassement_simple());
		    joueur.setClassementDouble(j.getClassement_double());
		    joueur.setClassementMixte(j.getClassement_mixte());
		    joueur.setCoteSimple(
			    (int) Float.parseFloat(j.getCote_simple()));
		    joueur.setCoteDouble(
			    (int) Float.parseFloat(j.getCote_double()));
		    joueur.setCoteMixte(
			    (int) Float.parseFloat(j.getCote_mixte()));

		}

		List<RolePOJO> listeRole = new ArrayList<>();
		RolePOJO role = this.managerUserService.getRole(2);
		listeRole.add(role);
		UserPOJO user = new UserPOJO();
		user.setNom(d.getLicence());
		user.setMdp(fr.max.lvdc.core.Utils.encodeBCrypt("LVDC"));
		user.setJoueur(joueur);
		user.setListeRoles(listeRole);

		listeUserToSave.add(user);

	    }

	    return true;

	} catch (

	final Exception e) {
	    e.printStackTrace();

	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "veuillez upload un fichier", " "));

	    return false;

	} finally {

	    if (fileExcel != null) {
		try {
		    fileExcel.close();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	    }

	}
    }
}


package fr.max.lvdc.facade.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserAuthenticate;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.facade.api.IUserManagerFacade;
import fr.max.lvdc.service.api.IManagerUserService;

@Service("userManagerFacade")
@Transactional(propagation = Propagation.REQUIRED)
public class UserManagerFacade
	implements IUserManagerFacade, UserDetailsService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER =
	    Logger.getLogger(UserManagerFacade.class);

    // DEBUT INJECTION SPRING.

    @Autowired
    private IManagerUserService managerUserService;

    // FIN INJECTION SPRING.

    /**
     * Locates the user based on the username. In the actual implementation, the
     * search may possibly be case insensitive, or case insensitive depending on
     * how the implementation instance is configured. In this case, the
     * <code>UserDetails</code> object that comes back may have a username that
     * is of a different case than what was actually requested..
     * 
     * @param login
     *            String
     * @return User user
     */
    @SuppressWarnings("unused")
    @Override
    public User loadUserByUsername(final String username) {

	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("Recherche du USER");
	}

	UserPOJO user = null;

	// Recherche du USER.
	try {

	    String password = null;

	    user = managerUserService.getUserByLogin(username);
	    password = user.getMdp();

	    if (user != null) {

		return new UserAuthenticate(user.getNom(), password, true,
			true, true, true, user.getAuthorities(),
			user.getJoueur());

	    }

	} catch (final Exception e) {
	    if (LOGGER.isInfoEnabled()) {
		LOGGER.info("Erreur Authentication : ", e);
	    }
	    throw new UsernameNotFoundException(e.getMessage(), e);
	}

	return null;

    }

    @Override
    public void saveMonCompte(final UserPOJO utilisateur) {
	this.managerUserService.saveMonCompte(utilisateur);
    }

    @Override
    public List<UserPOJO> findAllUsers() {
	return this.managerUserService.findAllUser();
    }

    @Override
    public UserPOJO findUserByCode(final String username,
	    final boolean initialize) {
	final UserPOJO user =
		this.managerUserService.getUserByLogin(username);
	return user;
    }

    @Override
    public List<RolePOJO> findAllRole() {
	return this.managerUserService.findAllRole();
    }

    @Override
    public RolePOJO getRole(int idRole) {
	return this.managerUserService.getRole(idRole);
    }

    @Override
    public UserPOJO findUserByJoueur(Integer idJoueur) {
	return this.managerUserService.findUserByJoueur(idJoueur);
    }

    @Override
    public UserPOJO findUserById(Integer idUtilisateur) {
	return this.managerUserService.findUserById(idUtilisateur);
    }
    
    @Override
    public void deleteUser(UserPOJO user) {
	this.managerUserService.deleteUser(user);
    }
}

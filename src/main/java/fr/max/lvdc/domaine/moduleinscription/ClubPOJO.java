package fr.max.lvdc.domaine.moduleinscription;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import fr.max.lvdc.core.ConstantValidation;
import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

@Entity
@Table(name = "club")
public class ClubPOJO extends AbstractObjetCreaMAJ {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1156809493437002849L;

	@NotNull
	@Size(max = ConstantValidation.MAXSIZE100)
	private String nomClub;

	@Size(max = ConstantValidation.MAXSIZE30)
	private String villeClub;

	@Size(max = ConstantValidation.MAXSIZE30)
	private String abrevClub;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_club")
	public Integer getId() {
		return super.getId();
	}

	@Column(name = "nom_club")
	public String getNomClub() {
		return nomClub;
	}

	public void setNomClub(String nomClub) {
		this.nomClub = nomClub;
	}
	
	
	@Column(name = "ville_club")
	public String getVilleClub() {
		return villeClub;
	}

	public void setVilleClub(String villeClub) {
		this.villeClub = villeClub;
	}


	@Column(name = "abrev_club")
	public String getAbrevClub() {
		return abrevClub;
	}

	public void setAbrevClub(String abrevClub) {
		this.abrevClub = abrevClub;
	}

	/**
	 * @return String getLabel
	 */
	@Transient
	public String getLabel() {

		final StringBuilder result = new StringBuilder();

		if (this.abrevClub != null) {
			result.append(this.abrevClub);
		}

		if (this.villeClub != null) {

			result.append(" - ");
			result.append(this.villeClub);
		}

		return result.toString();
	}
}

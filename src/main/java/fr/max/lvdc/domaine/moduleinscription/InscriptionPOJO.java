
package fr.max.lvdc.domaine.moduleinscription;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

@Entity
@Table(name = "inscription")
public class InscriptionPOJO extends AbstractObjetCreaMAJ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4171989156169068404L;

	private JoueurPOJO joueur;
	private TournoiPOJO tournoi;

	private boolean categorieSimple;
	private boolean categorieDouble;
	private boolean categorieMixte;
	private boolean categorieVeteran;

	private JoueurPOJO partenaireDouble;

	private JoueurPOJO partenaireMixte;

	private Date dateInscription;

	private boolean paye = false;

	private Integer prix;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_inscription")
	public Integer getId() {
		return super.getId();
	}

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "oid_joueur")
	public JoueurPOJO getJoueur() {
		return joueur;
	}

	public void setJoueur(JoueurPOJO joueur) {
		this.joueur = joueur;
	}

	@ManyToOne
	@JoinColumn(name = "oid_tournoi")
	public TournoiPOJO getTournoi() {
		return tournoi;
	}

	public void setTournoi(TournoiPOJO tournoi) {
		this.tournoi = tournoi;
	}

	@Column(name = "categorie_simple", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isCategorieSimple() {
		return categorieSimple;
	}

	public void setCategorieSimple(boolean categorieSimple) {
		this.categorieSimple = categorieSimple;
	}

	@Column(name = "categorie_veteran", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isCategorieVeteran() {
		return categorieVeteran;
	}

	public void setCategorieVeteran(boolean categorieVeteran) {
		this.categorieVeteran = categorieVeteran;
	}

	@Column(name = "categorie_double", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isCategorieDouble() {
		return categorieDouble;
	}

	public void setCategorieDouble(boolean categorieDouble) {
		this.categorieDouble = categorieDouble;
	}

	@Column(name = "paye", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isPaye() {
		return paye;
	}

	public void setPaye(boolean paye) {
		this.paye = paye;
	}

	@Column(name = "categorie_mixte", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isCategorieMixte() {
		return categorieMixte;
	}

	public void setCategorieMixte(boolean categorieMixte) {
		this.categorieMixte = categorieMixte;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_inscription")
	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	@ManyToOne
	@JoinColumn(name = "partenaire_double")
	public JoueurPOJO getPartenaireDouble() {
		return partenaireDouble;
	}

	public void setPartenaireDouble(JoueurPOJO partenaireDouble) {
		this.partenaireDouble = partenaireDouble;
	}

	@ManyToOne
	@JoinColumn(name = "partenaire_mixte")
	public JoueurPOJO getPartenaireMixte() {
		return partenaireMixte;
	}

	public void setPartenaireMixte(JoueurPOJO partenaireMixte) {
		this.partenaireMixte = partenaireMixte;
	}

	@Column(name = "prix_inscription")
	public Integer getPrix() {
		return prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	@Transient
	public Integer getNbTableau() {
		int x = 0;

		if (isCategorieSimple()) {
			x = x + 1;
		}

		if (isCategorieDouble()) {
			x = x + 1;
		}

		if (isCategorieMixte()) {
			x = x + 1;
		}

		return x;
	}

}


package fr.max.lvdc.domaine.moduleinscription;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import fr.max.lvdc.core.ConstantValidation;
import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

/**
 * @author max
 */
@Entity
@Table(name = "joueur")
public class JoueurPOJO extends AbstractObjetCreaMAJ {

    /**
     * 
     */
    private static final long serialVersionUID = 7045140250029398059L;

    @NotNull
    @Size(max = ConstantValidation.MAXSIZE30)
    private String nomJoueur;

    @Digits(integer = ConstantValidation.MAXSIZE2,
	    fraction = ConstantValidation.MAXSIZE0)
    private Integer ageJoueur;

    @NotNull
    @Size(max = ConstantValidation.MAXSIZE30)
    private String sexeJoueur;

    @Size(max = ConstantValidation.MAXSIZE30)
    private String licenceJoueur;

    private ClubPOJO club;

    private Integer tournoiPaye;
    private Integer tourGardoisPaye;

    private VeteranPOJO veteran;

    private boolean externe = false;

    private List<InscriptionPOJO> listeInscriptions = null;

    private UserPOJO user;

    private String classementSimple;

    private String classementDouble;

    private String classementMixte;

    private Integer coteSimple;
    private Integer coteDouble;
    private Integer coteMixte;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_joueur")
    public Integer getId() {
	return super.getId();
    }

    public JoueurPOJO() {
	this.listeInscriptions = new ArrayList<InscriptionPOJO>();
    }

    @Column(name = "nom")
    public String getNomJoueur() {
	return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
	this.nomJoueur = nomJoueur;
    }

    @Column(name = "age")
    public Integer getAgeJoueur() {
	return ageJoueur;
    }

    public void setAgeJoueur(Integer ageJoueur) {
	this.ageJoueur = ageJoueur;
    }

    @Column(name = "sexe")
    public String getSexeJoueur() {
	return sexeJoueur;
    }

    public void setSexeJoueur(String sexeJoueur) {
	this.sexeJoueur = sexeJoueur;
    }

    @Column(name = "externe", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isExterne() {
	return externe;
    }

    public void setExterne(boolean externe) {
	this.externe = externe;
    }

    @Column(name = "licence")
    public String getLicenceJoueur() {
	return licenceJoueur;
    }

    public void setLicenceJoueur(String licenceJoueur) {
	this.licenceJoueur = licenceJoueur;
    }

    @ManyToOne
    @JoinColumn(name = "oid_club")
    public ClubPOJO getClub() {
	return club;
    }

    public void setClub(ClubPOJO club) {
	this.club = club;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "oid_joueur")
    @Fetch(value = FetchMode.SUBSELECT)
    public List<InscriptionPOJO> getListeInscriptions() {
	return listeInscriptions;
    }

    public void
	    setListeInscriptions(List<InscriptionPOJO> listeInscriptions) {
	this.listeInscriptions = listeInscriptions;
    }

    @OneToOne(mappedBy = "joueur", cascade = CascadeType.ALL,
	    fetch = FetchType.LAZY, optional = false)
    public UserPOJO getUser() {
	return user;
    }

    public void setUser(UserPOJO user) {
	this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "oid_veteran")
    public VeteranPOJO getVeteran() {
	return veteran;
    }

    public void setVeteran(VeteranPOJO veteran) {
	this.veteran = veteran;
    }

    @Column(name = "tournoi_paye")
    public Integer getTournoiPaye() {
	return tournoiPaye;
    }

    public void setTournoiPaye(Integer tournoiPaye) {
	this.tournoiPaye = tournoiPaye;
    }

    @Column(name = "classement_simple")
    public String getClassementSimple() {
	return classementSimple;
    }

    public void setClassementSimple(String classementSimple) {
	this.classementSimple = classementSimple;
    }

    @Column(name = "classement_double")
    public String getClassementDouble() {
	return classementDouble;
    }

    public void setClassementDouble(String classementDouble) {
	this.classementDouble = classementDouble;
    }

    @Column(name = "classement_mixte")
    public String getClassementMixte() {
	return classementMixte;
    }

    public void setClassementMixte(String classementMixte) {
	this.classementMixte = classementMixte;
    }

    @Column(name = "cote_simple")
    public Integer getCoteSimple() {
	return coteSimple;
    }

    public void setCoteSimple(Integer coteSimple) {
	this.coteSimple = coteSimple;
    }

    @Column(name = "cote_double")
    public Integer getCoteDouble() {
	return coteDouble;
    }

    public void setCoteDouble(Integer coteDouble) {
	this.coteDouble = coteDouble;
    }

    @Column(name = "cote_mixte")
    public Integer getCoteMixte() {
	return coteMixte;
    }

    public void setCoteMixte(Integer coteMixte) {
	this.coteMixte = coteMixte;
    }

    @Column(name = "tournoi_gardois_paye")
    public Integer getTourGardoisPaye() {
	return tourGardoisPaye;
    }

    public void setTourGardoisPaye(Integer tourGardoisPaye) {
	this.tourGardoisPaye = tourGardoisPaye;
    }

    /**
     * @return String getLabel
     */
    @Transient
    public String getLabel() {

	final StringBuilder result = new StringBuilder();

	if (this.nomJoueur != null) {
	    result.append(this.nomJoueur);
	}

	return result.toString();
    }

    @Transient
    public String getColorSimple() {

	final StringBuilder result = new StringBuilder();

	try {

	    if (this.classementSimple.contains("P")) {
		result.append("YellowBack");
	    } else if (this.classementSimple.contains("D")) {
		result.append("GreenBack");
	    } else if (this.classementSimple.contains("R")) {
		result.append("BlueBack");
	    } else if (this.classementSimple.contains("N")) {
		result.append("RedBack");
	    }

	} catch (NullPointerException npe) {
	    if (classementSimple == null) {
		result.append("WhiteBack");
	    }
	}

	return result.toString();
    }

    @Transient
    public String getColorDouble() {

	final StringBuilder result = new StringBuilder();

	try {

	    if (this.classementDouble.contains("P")) {
		result.append("YellowBack");
	    } else if (this.classementDouble.contains("D")) {
		result.append("GreenBack");
	    } else if (this.classementDouble.contains("R")) {
		result.append("BlueBack");
	    } else if (this.classementDouble.contains("N")) {
		result.append("RedBack");
	    }
	} catch (NullPointerException npe) {

	    if (classementDouble == null) {
		result.append("WhiteBack");
	    }
	}

	return result.toString();
    }

    @Transient
    public String getColorMixte() {

	final StringBuilder result = new StringBuilder();

	try {

	    if (this.classementMixte.contains("P")) {
		result.append("YellowBack");
	    } else if (this.classementMixte.contains("D")) {
		result.append("GreenBack");
	    } else if (this.classementMixte.contains("R")) {
		result.append("BlueBack");
	    } else if (this.classementMixte.contains("N")) {
		result.append("RedBack");
	    }

	} catch (NullPointerException npe) {

	    if (classementMixte == null) {
		result.append("WhiteBack");
	    }
	}

	return result.toString();
    }

    @Transient
    public Integer getNbInscriptions() {
	int x = 0;

	for (InscriptionPOJO i : this.listeInscriptions) {
	    x = x + 1;
	}

	return x;
    }

    @Transient
    public Integer getTotal() {
	int x = 0;
	for (InscriptionPOJO i : this.listeInscriptions) {
	    if (!i.isPaye()) {
		x = x + i.getPrix();
	    }
	}

	return x;
    }

    @Transient
    public Integer getTotalSansPaye() {
	int x = 0;
	for (InscriptionPOJO i : this.listeInscriptions) {

	    x = x + i.getPrix();

	}

	return x;
    }

    @Transient
    public String getClassementCompos() {
	StringBuilder string = new StringBuilder();

	string.append(this.classementSimple + "/" + this.classementDouble
		+ "/" + this.classementMixte);

	return string.toString();
    }

}

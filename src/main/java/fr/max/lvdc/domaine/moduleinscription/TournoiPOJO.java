
package fr.max.lvdc.domaine.moduleinscription;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import fr.max.lvdc.core.ConstantValidation;
import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

/**
 * @author max
 */
@Entity
@Table(name = "tournoi")
public class TournoiPOJO extends AbstractObjetCreaMAJ {

    /**
     * 
     */
    private static final long serialVersionUID = 3214130157043697597L;

    @NotNull
    @Size(max = ConstantValidation.MAXSIZE150)
    private String libelleTournoi;

    @NotNull
    private Date dateTournoi;

    @NotNull
    private Date dateLimiteInscription;

    @NotNull
    @Size(max = ConstantValidation.MAXSIZE30)
    private String lieuTournoi;

    @NotNull
    private Integer nombreJour;

    private String lien;

    private String detail;

    private Integer prixUnTableau;
    private Integer prixDeuxTableau;
    private Integer prixTroisTableau;

    private boolean tourGardois;

    private List<DocumentPOJO> listeDocument;

    private CategorieTournoiPOJO categorieTournoi;

    private List<InscriptionPOJO> listeInscriptions =
	    new ArrayList<InscriptionPOJO>();

    private Boolean scoreCenter;
    private String lienScoreCenter;

    private boolean n = false;
    private boolean r = false;
    private boolean d = false;
    private boolean p = false;

    private boolean isActif;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tournoi")
    public Integer getId() {
	return super.getId();
    }

    public TournoiPOJO() {
	this.categorieTournoi = new CategorieTournoiPOJO();
	this.listeDocument = new ArrayList<DocumentPOJO>();
	this.listeInscriptions = new ArrayList<InscriptionPOJO>();
    }

    @Column(name = "libelle_tournoi")
    public String getLibelleTournoi() {
	return libelleTournoi;
    }

    public void setLibelleTournoi(String libelleTournoi) {
	this.libelleTournoi = libelleTournoi;
    }

    @Column(name = "detail")
    public String getDetail() {
	return detail;
    }

    public void setDetail(String detail) {
	this.detail = detail;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_tournoi")
    public Date getDateTournoi() {
	return dateTournoi;
    }

    public void setDateTournoi(Date dateTournoi) {
	this.dateTournoi = dateTournoi;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_limite_inscription")
    public Date getDateLimiteInscription() {
	return dateLimiteInscription;
    }

    public void setDateLimiteInscription(Date dateLimiteInscription) {
	this.dateLimiteInscription = dateLimiteInscription;
    }

    @Column(name = "lieu_tournoi")
    public String getLieuTournoi() {
	return lieuTournoi;
    }

    public void setLieuTournoi(String lieuTournoi) {
	this.lieuTournoi = lieuTournoi;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "oid_categorie_tournoi")
    public CategorieTournoiPOJO getCategorieTournoi() {
	return categorieTournoi;
    }

    public void setCategorieTournoi(CategorieTournoiPOJO categorieTournoi) {
	this.categorieTournoi = categorieTournoi;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tournoi")
    @Fetch(value = FetchMode.SUBSELECT)
    public List<InscriptionPOJO> getListeInscriptions() {
	return listeInscriptions;
    }

    public void
	    setListeInscriptions(List<InscriptionPOJO> listeInscriptions) {
	this.listeInscriptions = listeInscriptions;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "oid_tournoi")
    public List<DocumentPOJO> getListeDocument() {
	return listeDocument;
    }

    public void setListeDocument(List<DocumentPOJO> listeDocument) {
	this.listeDocument = listeDocument;
    }

    @Column(name = "nombre_jour")
    public Integer getNombreJour() {
	return nombreJour;
    }

    public void setNombreJour(Integer nombreJour) {
	this.nombreJour = nombreJour;
    }

    @Column(name = "prix_un_tableau")
    public Integer getPrixUnTableau() {
	return prixUnTableau;
    }

    public void setPrixUnTableau(Integer prixUnTableau) {
	this.prixUnTableau = prixUnTableau;
    }

    @Column(name = "prix_deux_tableau")
    public Integer getPrixDeuxTableau() {
	return prixDeuxTableau;
    }

    public void setPrixDeuxTableau(Integer prixDeuxTableau) {
	this.prixDeuxTableau = prixDeuxTableau;
    }

    @Column(name = "prix_trois_tableau")
    public Integer getPrixTroisTableau() {
	return prixTroisTableau;
    }

    public void setPrixTroisTableau(Integer prixTroisTableau) {
	this.prixTroisTableau = prixTroisTableau;
    }

    @Column(name = "lien")
    public String getLien() {
	return lien;
    }

    public void setLien(String lien) {
	this.lien = lien;
    }

    @Column(name = "n", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isN() {
	return n;
    }

    public void setN(boolean n) {
	this.n = n;
    }

    @Column(name = "r", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isR() {
	return r;
    }

    public void setR(boolean r) {
	this.r = r;
    }

    @Column(name = "d", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isD() {
	return d;
    }

    public void setD(boolean d) {
	this.d = d;
    }

    @Column(name = "p", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isP() {
	return p;
    }

    public void setP(boolean p) {
	this.p = p;
    }

    @Column(name = "tour_gardois", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isTourGardois() {
	return tourGardois;
    }

    public void setTourGardois(boolean tourGardois) {
	this.tourGardois = tourGardois;
    }

    @Column(name = "isActif", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isActif() {
	return isActif;
    }

    public void setActif(boolean isActif) {
	this.isActif = isActif;
    }

    @Column(name = "score_center", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public Boolean getScoreCenter() {
	return scoreCenter;
    }

    public void setScoreCenter(Boolean scoreCenter) {
	this.scoreCenter = scoreCenter;
    }

    @Column(name = "lien_score_center")
    public String getLienScoreCenter() {
	return lienScoreCenter;
    }

    public void setLienScoreCenter(String lienScoreCenter) {
	this.lienScoreCenter = lienScoreCenter;
    }

    @Transient
    public Boolean getLock() {

	if (dateLimiteInscription.after(new Date())) {
	    return true;
	}
	return false;
    }

    @Transient
    public Integer getNombre() {
	int x = 0;
	for (InscriptionPOJO inscription : this.listeInscriptions) {
	    x++;
	}
	return x;
    }

    @Transient
    public String getDateString() {

	Date date = new Date();
	Calendar cal = Calendar.getInstance();
	cal.setTime(dateTournoi);
	cal.add(Calendar.DAY_OF_MONTH, +1);
	date = cal.getTime();

	String pattern = "dd-MM-yyyy";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	String dateT = simpleDateFormat.format(dateTournoi);
	String dateAdd = simpleDateFormat.format(date);

	StringBuilder label = new StringBuilder();
	label.append("du ");
	label.append(dateT);
	label.append(" au ");
	label.append(dateAdd);

	return label.toString();

    }

    @Transient
    public String getLabel() {

	String pattern = "dd-MM-yyyy";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String dateT = simpleDateFormat.format(dateTournoi);

	StringBuilder label = new StringBuilder();

	if (lieuTournoi != null) {
	    label.append(this.lieuTournoi);
	}

	label.append(" le ");

	if (dateTournoi != null) {
	    label.append(dateT);
	}

	return label.toString();
    }

    @Transient
    public String getLabelPrix() {

	StringBuilder label = new StringBuilder();

	label.append("1 tableau = " + prixUnTableau.toString() + "e");

	if (prixDeuxTableau != null) {
	    label.append(
		    " - 2 tableaux = " + prixDeuxTableau.toString() + "e");
	}

	if (prixTroisTableau != null) {
	    label.append(
		    " - 3 tabeaux = " + prixTroisTableau.toString() + "e");
	}

	return label.toString();

    }

    @Transient
    public Integer getNbTableau() {
	int x = 0;
	if (prixUnTableau != null) {
	    x = x + 1;
	}

	if (prixDeuxTableau != null) {
	    x = x + 1;
	}

	if (prixTroisTableau != null) {
	    x = x + 1;
	}

	return x;
    }
}

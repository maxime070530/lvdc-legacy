
package fr.max.lvdc.domaine.moduleinscription;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

@Entity
@Table(name = "categorie_tournoi")
public class CategorieTournoiPOJO extends AbstractObjetCreaMAJ {

    /**
     * 
     */
    private static final long serialVersionUID = 706771962464101317L;

    private boolean categorieJeune = false;

    private boolean categorieSenior = false;

    private boolean categorieVeteran = false;

    private boolean disciplineSimple = false;

    private boolean disciplineDouble = false;

    private boolean disciplineMixte = false;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_categorie_tournoi")
    public Integer getId() {
	return super.getId();
    }

    @Column(name = "jeune", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isCategorieJeune() {
	return categorieJeune;
    }

    public void setCategorieJeune(boolean categorieJeune) {
	this.categorieJeune = categorieJeune;
    }

    @Column(name = "senior", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isCategorieSenior() {
	return categorieSenior;
    }

    public void setCategorieSenior(boolean categorieSenior) {
	this.categorieSenior = categorieSenior;
    }

    @Column(name = "veteran", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isCategorieVeteran() {
	return categorieVeteran;
    }

    public void setCategorieVeteran(boolean categorieVeteran) {
	this.categorieVeteran = categorieVeteran;
    }

    @Column(name = "discipline_simple", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isDisciplineSimple() {
	return disciplineSimple;
    }

    public void setDisciplineSimple(boolean disciplineSimple) {
	this.disciplineSimple = disciplineSimple;
    }

    @Column(name = "discipline_double", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isDisciplineDouble() {
	return disciplineDouble;
    }

    public void setDisciplineDouble(boolean disciplineDouble) {
	this.disciplineDouble = disciplineDouble;
    }

    @Column(name = "discipline_mixte", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isDisciplineMixte() {
	return disciplineMixte;
    }

    public void setDisciplineMixte(boolean disciplineMixte) {
	this.disciplineMixte = disciplineMixte;
    }

    @Transient
    public String getLabel() {

	StringBuilder label = new StringBuilder();

	if (this.categorieSenior == true) {
	    label.append("Tournoi senior");
	} else if (this.categorieVeteran == true) {
	    label.append("Tournoi veteran");
	} else if (this.categorieSenior == true
		&& this.categorieVeteran == true) {
	    label.append("Tournoi senior et veteran");
	}

	return label.toString();

    }

    // @Transient
    // public String getDiscipline() {
    // StringBuilder disci = new StringBuilder();
    //
    // if (this.disciplineSimple == true) {
    // disci.append("Categorie Simple");
    // } else if (this.disciplineDouble == true) {
    // disci.append("Categorie Double");
    // } else if (this.disciplineMixte == true) {
    // disci.append("Categorie Mixte");
    // }
    //
    // }
}

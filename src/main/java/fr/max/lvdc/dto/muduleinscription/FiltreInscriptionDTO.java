
package fr.max.lvdc.dto.muduleinscription;

import java.util.Date;

public class FiltreInscriptionDTO {

    private String nomJoueur;

    private String libelleTournoi;

    private Date dateInscription;
    private Date dateTournoi;

    private Integer prixInscription;

    private String lieuTournoi;

    private String licenceJoueur;

    private Integer paye;

    private Integer intervalle = 1;

    private Boolean interne;

    public String getNomJoueur() {
	return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
	this.nomJoueur = nomJoueur;
    }

    public String getLibelleTournoi() {
	return libelleTournoi;
    }

    public void setLibelleTournoi(String libelleTournoi) {
	this.libelleTournoi = libelleTournoi;
    }

    public Date getDateInscription() {
	return dateInscription;
    }

    public void setDateInscription(Date dateInscription) {
	this.dateInscription = dateInscription;
    }

    public Date getDateTournoi() {
	return dateTournoi;
    }

    public void setDateTournoi(Date dateTournoi) {
	this.dateTournoi = dateTournoi;
    }

    public Integer getPrixInscription() {
	return prixInscription;
    }

    public void setPrixInscription(Integer prixInscription) {
	this.prixInscription = prixInscription;
    }

    public String getLieuTournoi() {
	return lieuTournoi;
    }

    public void setLieuTournoi(String lieuTournoi) {
	this.lieuTournoi = lieuTournoi;
    }

    public String getLicenceJoueur() {
	return licenceJoueur;
    }

    public void setLicenceJoueur(String licenceJoueur) {
	this.licenceJoueur = licenceJoueur;
    }

    public Integer getPaye() {
	return paye;
    }

    public void setPaye(Integer paye) {
	this.paye = paye;
    }

    public Integer getIntervalle() {
	return intervalle;
    }

    public void setIntervalle(Integer intervalle) {
	this.intervalle = intervalle;
    }

    public Boolean getInterne() {
	return interne;
    }

    public void setInterne(Boolean interne) {
	this.interne = interne;
    }

}

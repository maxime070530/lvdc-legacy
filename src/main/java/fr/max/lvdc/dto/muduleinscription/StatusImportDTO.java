
package fr.max.lvdc.dto.muduleinscription;

public class StatusImportDTO {

	private int nbrLigneTraiter = -1;
	private int totalLigneTraiter = -1;

	public int getNbrLigneTraiter() {
		return nbrLigneTraiter;
	}

	public void setNbrLigneTraiter(int nbrLigneTraiter) {
		this.nbrLigneTraiter = nbrLigneTraiter;
	}

	public int getTotalLigneTraiter() {
		return totalLigneTraiter;
	}

	public void setTotalLigneTraiter(int totalLigneTraiter) {
		this.totalLigneTraiter = totalLigneTraiter;
	}

}

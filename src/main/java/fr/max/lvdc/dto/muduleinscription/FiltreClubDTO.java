package fr.max.lvdc.dto.muduleinscription;

public class FiltreClubDTO {

	private String nomClub;
	private String villeClub;
	private String abrevClub;

	public String getNomClub() {
		return nomClub;
	}

	public void setNomClub(String nomClub) {
		this.nomClub = nomClub;
	}

	public String getVilleClub() {
		return villeClub;
	}

	public void setVilleClub(String villeClub) {
		this.villeClub = villeClub;
	}

	public String getAbrevClub() {
		return abrevClub;
	}

	public void setAbrevClub(String abrevClub) {
		this.abrevClub = abrevClub;
	}

}


package fr.max.lvdc.dto.muduleinscription;

public class ImportDataDTO {

    private String licence;

    public String getLicence() {
	return licence;
    }

    public void setLicence(String licence) {
	this.licence = licence;
    }

}

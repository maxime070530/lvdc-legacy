
package fr.max.lvdc.dto.muduleinscription;

import java.util.Date;

public class FiltreJoueurDTO {

    private String nomJoueur;

    private String licenceJoueur;
    private String sexe;
    private Boolean interne;
    private Boolean inscriptionNull = false;
    private Integer intervalle = 1;
    private Boolean conpta = false;

    private Date dateDebut;

    public String getNomJoueur() {
	return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
	this.nomJoueur = nomJoueur;
    }

    public String getLicenceJoueur() {
	return licenceJoueur;
    }

    public void setLicenceJoueur(String licenceJoueur) {
	this.licenceJoueur = licenceJoueur;
    }

    public String getSexe() {
	return sexe;
    }

    public void setSexe(String sexe) {
	this.sexe = sexe;
    }

    public Boolean getInterne() {
	return interne;
    }

    public void setInterne(Boolean interne) {
	this.interne = interne;
    }

    public Boolean getInscriptionNull() {
	return inscriptionNull;
    }

    public void setInscriptionNull(Boolean inscriptionNull) {
	this.inscriptionNull = inscriptionNull;
    }

    public Date getDateDebut() {
	return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
	this.dateDebut = dateDebut;
    }

    public Integer getIntervalle() {
	return intervalle;
    }

    public void setIntervalle(Integer intervalle) {
	this.intervalle = intervalle;
    }

    public Boolean getConpta() {
	return conpta;
    }

    public void setConpta(Boolean conpta) {
	this.conpta = conpta;
    }

}

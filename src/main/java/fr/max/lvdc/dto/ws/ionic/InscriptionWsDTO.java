
package fr.max.lvdc.dto.ws.ionic;

import java.util.Date;

public class InscriptionWsDTO {

    private JoueurWsUnanimDTO joueur;

    private boolean categorieSimple;
    private boolean categorieDouble;
    private boolean categorieMixte;
    private boolean categorieVeteran;

    private JoueurWsUnanimDTO partenaireDouble;

    private JoueurWsUnanimDTO partenaireMixte;

    private Date dateInscription;

    private boolean paye = false;

    private Integer prix;

    public boolean isCategorieSimple() {
	return categorieSimple;
    }

    public void setCategorieSimple(boolean categorieSimple) {
	this.categorieSimple = categorieSimple;
    }

    public boolean isCategorieDouble() {
	return categorieDouble;
    }

    public void setCategorieDouble(boolean categorieDouble) {
	this.categorieDouble = categorieDouble;
    }

    public boolean isCategorieMixte() {
	return categorieMixte;
    }

    public void setCategorieMixte(boolean categorieMixte) {
	this.categorieMixte = categorieMixte;
    }

    public boolean isCategorieVeteran() {
	return categorieVeteran;
    }

    public void setCategorieVeteran(boolean categorieVeteran) {
	this.categorieVeteran = categorieVeteran;
    }

    public JoueurWsUnanimDTO getPartenaireDouble() {
	return partenaireDouble;
    }

    public void setPartenaireDouble(JoueurWsUnanimDTO partenaireDouble) {
	this.partenaireDouble = partenaireDouble;
    }

    public JoueurWsUnanimDTO getPartenaireMixte() {
	return partenaireMixte;
    }

    public void setPartenaireMixte(JoueurWsUnanimDTO partenaireMixte) {
	this.partenaireMixte = partenaireMixte;
    }

    public Date getDateInscription() {
	return dateInscription;
    }

    public void setDateInscription(Date dateInscription) {
	this.dateInscription = dateInscription;
    }

    public boolean isPaye() {
	return paye;
    }

    public void setPaye(boolean paye) {
	this.paye = paye;
    }

    public Integer getPrix() {
	return prix;
    }

    public void setPrix(Integer prix) {
	this.prix = prix;
    }

    public JoueurWsUnanimDTO getJoueur() {
	return joueur;
    }

    public void setJoueur(JoueurWsUnanimDTO joueur) {
	this.joueur = joueur;
    }

}

package fr.max.lvdc.dto.ws;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JoueurWsDTO {

	private String licence;
	private String personID;
	private String nom;

	@JsonIgnore
	public String FirstName;

	@JsonIgnore
	public String LastName;

	private String club;

	private String club_court;

	private String cote_simple;

	private String cote_double;

	private String cote_mixte;
	private String classement_simple;
	private String classement_double;
	private String classement_mixte;
	private String sexe;

	private String saison;

	public String getClub_court() {
		return club_court;
	}

	public void setClub_court(String club_court) {
		this.club_court = club_court;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getClub() {
		return club;
	}

	public void setClub(String club) {
		this.club = club;
	}

	public String getCote_simple() {
		return cote_simple;
	}

	public void setCote_simple(String cote_simple) {
		this.cote_simple = cote_simple;
	}

	public String getCote_double() {
		return cote_double;
	}

	public void setCote_double(String cote_double) {
		this.cote_double = cote_double;
	}

	public String getCote_mixte() {
		return cote_mixte;
	}

	public void setCote_mixte(String cote_mixte) {
		this.cote_mixte = cote_mixte;
	}

	public String getClassement_simple() {
		return classement_simple;
	}

	public void setClassement_simple(String classement_simple) {
		this.classement_simple = classement_simple;
	}

	public String getClassement_double() {
		return classement_double;
	}

	public void setClassement_double(String classement_double) {
		this.classement_double = classement_double;
	}

	public String getClassement_mixte() {
		return classement_mixte;
	}

	public void setClassement_mixte(String classement_mixte) {
		this.classement_mixte = classement_mixte;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getPersonID() {
		return personID;
	}

	public void setPersonID(String personID) {
		this.personID = personID;
	}

	public String getLabel() {
		StringBuilder string = new StringBuilder();

		string.append(nom);
		string.append(" - " + classement_simple + "/" + classement_double + "/" + classement_mixte);
		string.append(" - " + club_court);

		return string.toString();
	}

	public String getSaison() {
		return saison;
	}

	public void setSaison(String saison) {
		this.saison = saison;
	}

}


package fr.max.lvdc.dto.ws.ionic;

public class JoueurWsUnanimDTO {

    private String nomJoueur;

    private String sexeJoueur;

    private String licenceJoueur;

    private Integer tournoiPaye;
    private Integer tourGardoisPaye;

    private boolean externe = false;

    // private UserPOJO user;

    private String classementSimple;

    private String classementDouble;

    private String classementMixte;

    private Integer coteSimple;
    private Integer coteDouble;
    private Integer coteMixte;

    private String nomClub;

    private String abrevClub;

    public String getNomJoueur() {
	return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
	this.nomJoueur = nomJoueur;
    }

    public String getSexeJoueur() {
	return sexeJoueur;
    }

    public void setSexeJoueur(String sexeJoueur) {
	this.sexeJoueur = sexeJoueur;
    }

    public String getLicenceJoueur() {
	return licenceJoueur;
    }

    public void setLicenceJoueur(String licenceJoueur) {
	this.licenceJoueur = licenceJoueur;
    }

    public Integer getTournoiPaye() {
	return tournoiPaye;
    }

    public void setTournoiPaye(Integer tournoiPaye) {
	this.tournoiPaye = tournoiPaye;
    }

    public Integer getTourGardoisPaye() {
	return tourGardoisPaye;
    }

    public void setTourGardoisPaye(Integer tourGardoisPaye) {
	this.tourGardoisPaye = tourGardoisPaye;
    }

    public boolean isExterne() {
	return externe;
    }

    public void setExterne(boolean externe) {
	this.externe = externe;
    }

    public String getClassementSimple() {
	return classementSimple;
    }

    public void setClassementSimple(String classementSimple) {
	this.classementSimple = classementSimple;
    }

    public String getClassementDouble() {
	return classementDouble;
    }

    public void setClassementDouble(String classementDouble) {
	this.classementDouble = classementDouble;
    }

    public String getClassementMixte() {
	return classementMixte;
    }

    public void setClassementMixte(String classementMixte) {
	this.classementMixte = classementMixte;
    }

    public Integer getCoteSimple() {
	return coteSimple;
    }

    public void setCoteSimple(Integer coteSimple) {
	this.coteSimple = coteSimple;
    }

    public Integer getCoteDouble() {
	return coteDouble;
    }

    public void setCoteDouble(Integer coteDouble) {
	this.coteDouble = coteDouble;
    }

    public Integer getCoteMixte() {
	return coteMixte;
    }

    public void setCoteMixte(Integer coteMixte) {
	this.coteMixte = coteMixte;
    }

    public String getNomClub() {
	return nomClub;
    }

    public void setNomClub(String nomClub) {
	this.nomClub = nomClub;
    }

    public String getAbrevClub() {
	return abrevClub;
    }

    public void setAbrevClub(String abrevClub) {
	this.abrevClub = abrevClub;
    }

}


package webservice;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public abstract class ApiResponse {

	private HttpStatus status;
	private Integer code;
	private EnumSeverity severity = EnumSeverity.error;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	private String message;

	public ApiResponse(HttpStatus status, EnumSeverity severity) {
		this.status = status;
		this.code = status.value();
	}

	public ApiResponse(HttpStatus status, EnumSeverity severity, Throwable ex) {
		this(status, severity);
		this.status = status;
		this.code = status.value();
		this.message = "Unexpected error";
	}

	public ApiResponse(HttpStatus status, EnumSeverity severity, String message, Throwable ex) {
		this(status, severity);
		this.status = status;
		this.code = status.value();
		this.message = message;
	}

	public EnumSeverity getSeverity() {
		return severity;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public int getCode() {
		return code;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}


package webservice.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ListResourceBundle;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.dto.ws.ionic.InscriptionWsDTO;
import fr.max.lvdc.dto.ws.ionic.JoueurWsUnanimDTO;
import fr.max.lvdc.dto.ws.ionic.TournoiWsDTO;
import fr.max.lvdc.facade.api.ICommunFacade;
import webservice.api.IUnanimWS;

@Path("/unanim")
@Component("UnanimWS")
public class UnanimWS implements IUnanimWS {

	@Context
	ServletContext servletContext;

	@Autowired
	private ICommunFacade communFacade;

	private final DateFormat sdfDatePattern = new SimpleDateFormat("YYYY-MM-dd%20HH:mm:ss");

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all-tournoi")
	@Override
	public Response findAllTournois() {

		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ICommunFacade communFacade = ctx.getBean("communFacade", ICommunFacade.class);

		List<TournoiPOJO> listeTournois = communFacade.findAllTournoiByDate();
		List<TournoiWsDTO> listeTournoisWs = new ArrayList<>();

		for (TournoiPOJO tournoi : listeTournois) {

			TournoiWsDTO tournoiWs = new TournoiWsDTO();

			tournoiWs.setLibelleTournoi(tournoi.getLibelleTournoi());
			tournoiWs.setDateTournoi(sdfDatePattern.format(tournoi.getDateTournoi()));
			tournoiWs.setDateLimiteInscription(sdfDatePattern.format(tournoi.getDateLimiteInscription()));
			tournoiWs.setLieuTournoi(tournoi.getLieuTournoi());
			tournoiWs.setNombreJour(tournoi.getNombreJour());
			tournoiWs.setDetail(tournoi.getDetail());
			tournoiWs.setPrixUnTableau(tournoi.getPrixUnTableau());
			tournoiWs.setPrixDeuxTableau(tournoi.getPrixDeuxTableau());
			tournoiWs.setPrixTroisTableau(tournoi.getPrixTroisTableau());
			tournoiWs.setTourGardois(tournoi.isTourGardois());
			tournoiWs.setCategorieJeune(tournoi.getCategorieTournoi().isCategorieJeune());
			tournoiWs.setCategorieSenior(tournoi.getCategorieTournoi().isCategorieSenior());
			tournoiWs.setCategorieVeteran(tournoi.getCategorieTournoi().isCategorieVeteran());
			tournoiWs.setDisciplineSimple(tournoi.getCategorieTournoi().isDisciplineSimple());
			tournoiWs.setDisciplineDouble(tournoi.getCategorieTournoi().isDisciplineDouble());
			tournoiWs.setDisciplineMixte(tournoi.getCategorieTournoi().isDisciplineMixte());

			listeTournoisWs.add(tournoiWs);
		}

		return Response.status(Response.Status.OK).entity(listeTournoisWs).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all-joueurs")
	@Override
	public Response findAllJoueurs() {

		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ICommunFacade communFacade = ctx.getBean("communFacade", ICommunFacade.class);

		List<JoueurPOJO> listeJoueurs = communFacade.findAllJoueur();
		List<JoueurWsUnanimDTO> listeJoueursWS = new ArrayList<JoueurWsUnanimDTO>();

		for (JoueurPOJO joueur : listeJoueurs) {
			JoueurWsUnanimDTO joueurWs = new JoueurWsUnanimDTO();

			joueurWs.setNomJoueur(joueur.getNomJoueur());
			joueurWs.setSexeJoueur(joueur.getSexeJoueur());
			joueurWs.setLicenceJoueur(joueur.getLicenceJoueur());

			listeJoueursWS.add(joueurWs);
			
		}

		return Response.status(Response.Status.OK).entity(listeJoueursWS).build();

	}
}

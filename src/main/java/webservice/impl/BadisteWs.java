
package webservice.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.ws.JoueurWsDTO;
import fr.max.lvdc.facade.api.ICommunFacade;
import webservice.api.IBadisteWS;

@Path("/badiste")
@Component("BadisteWS")
public class BadisteWs implements IBadisteWS {

	private final static String URL_API_REST = "https://www.obc-nimes.com/api/";

	private final static String JOUEUR_PATH = "joueur";

	DateTimeFormatter sdfDatePattern = DateTimeFormatter.ofPattern("YYYY-MM-dd%20HH:mm:ss");

	@Context
	ServletContext servletContext;

	@Autowired
	private ICommunFacade communFacade;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all-joueurs-by-club")
	@Override
	public Response findAllJoueursByClub() throws URISyntaxException {

		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ICommunFacade communFacade = ctx.getBean("communFacade", ICommunFacade.class);

		List<JoueurPOJO> listeJoueursss = this.communFacade.findAllJoueur();
		List<JoueurWsDTO> listeJoueurs = new ArrayList<JoueurWsDTO>();
		for (JoueurPOJO joueur : listeJoueursss) {

			CloseableHttpClient httpclient = HttpClients.createDefault();

			URIBuilder url = null;
			url = new URIBuilder(URL_API_REST + JOUEUR_PATH);
			url.addParameter("query", joueur.getLicenceJoueur());

			final HttpGet getRequest = new HttpGet(url.build());

			getRequest.setHeader("Accept", "application/json");
			getRequest.setHeader("Content-type", "application/json");

			HttpResponse response = null;
			HttpEntity entity = null;

			try {
				response = httpclient.execute(getRequest);
				entity = response.getEntity();
				ObjectMapper mapper = new ObjectMapper();

				JsonNode root = mapper.readTree(entity.getContent());
				httpclient.close();

				List<JoueurWsDTO> joueurs = mapper.readValue(root.traverse(),
						mapper.getTypeFactory().constructCollectionType(List.class, JoueurWsDTO.class));

				for (JoueurWsDTO j : joueurs) {
					listeJoueurs.add(j);
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return Response.status(Response.Status.OK).entity(listeJoueurs).build();
	}

}

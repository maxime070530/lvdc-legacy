
package webservice;

import org.springframework.http.HttpStatus;

public class ApiSuccesResponse extends ApiResponse {

    public ApiSuccesResponse(HttpStatus status) {
	super(status, EnumSeverity.succes);
    }

}
